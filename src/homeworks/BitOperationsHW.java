package homeworks;

public class BitOperationsHW {
    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString(62));  //00111110
        System.out.println(Integer.toBinaryString(13));  //00001101
        System.out.println(Integer.toBinaryString(54));  //00110110
        System.out.println(Integer.toBinaryString(17));  //00010001
        System.out.println("62 & 13 = " + (62 & 13));       //00001100
        System.out.println("62 | 13 = " + (62 | 13));       //00111111
        System.out.println("62 >> 13 = " + (62 >> 2));      //00111111 = 00001111 = 62 / 2 (2) = 62/4 = 15
        System.out.println("62 << 13 = " + (62 << 2));      //00111111 = 11111100 = 62 * 2 (2) = 62*4 = 248
        System.out.println("54 & 17 = " + (54 & 17));       //00010000
        System.out.println("54 | 17 = " + (54 | 17));       //00110111



    }
}
