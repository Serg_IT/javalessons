package homeworks.post_server;

public class TestPostServer {
    public static void main(String[] args) {
        MailService mailService = new MailService();

        MailBox mailBox1 = new MailBox("qqq", NameMailBox.RAMBLER);
        MailBox mailBox6 = new MailBox("My", NameMailBox.YANDEX);//messages nulls
        MailBox mailBox7 = new MailBox("bla", NameMailBox.GMAIL);
        mailService.addMailBox(mailBox6);
        mailService.addMailBox(mailBox7);


        Message message = new Message("Good price","qqq");
        Message message1 = new Message("Hello my friend", "My");
        Message message3 = new Message("How are you?", "www");
        Message message4 = new Message("Hi", "My");
        Message message5 = new Message("The weather is fine.", "rrr");
        Message message6 = new Message("Tom is vaiting.", "eee");

//        mailService.addMessageToMailBox("My@erty", message);
        mailService.addMessageToMailBox(mailBox7.getName(), message1);
        mailService.addMessageToMailBox(mailBox6.getName(), message);
        mailService.addMessageToMailBox(mailBox1.getName(), message3);
        mailService.addMessageToMailBox("www", message4);
        mailService.addMessageToMailBox("ttt", message5);
        mailService.addMessageToMailBox("rrr", message5);
        mailService.addMessageToMailBox("eee22222", message6);
//        mailService.addMessageToMailBox1(my.getName(), "Good price", "yandex");

//        mailService.addMailBox(new MailBox("kjhg", NameMailBox.GMAIL));
        mailService.showAllMailBox();
//        mailService.showAllMessageInMailBox("rtyuio");
    }
}
