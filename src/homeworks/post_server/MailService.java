/*
* Организовать приложение почтового сервера.
Приложение должно позволять:
1) Создавать почтовый ящик, добавить проверку/защиту на дубликацию.
2) Организовать возможность отправки письма на конкретный адрес//from, to, message
3) выводить в консоль список существующих адресов.
4) Просмотр сообщения по конкретному адресу. // вывести массив сообщений из адреса
5) Организовать проверку/защиту отправки письма на несуществующий адрес.
* */
package homeworks.post_server;

import java.util.Objects;

public class MailService {
    private MailBox[] boxes;

    public MailService() {
        this.boxes = new MailBox[10];
        fillMailBox();
    }

    private void fillMailBox() {
        MailBox mailBox1 = new MailBox("qqq", NameMailBox.GMAIL);
        MailBox mailBox2 = new MailBox("www", NameMailBox.YANDEX);
        MailBox mailBox3 = new MailBox("eee", NameMailBox.RAMBLER);
        MailBox mailBox4 = new MailBox("rrr", NameMailBox.GMAIL);
        MailBox mailBox5 = new MailBox("ttt", NameMailBox.YANDEX);

        boxes[0] = mailBox1;
        boxes[1] = mailBox2;
        boxes[2] = mailBox3;
        boxes[3] = mailBox4;
        boxes[4] = mailBox5;
    }


    public void addMessageToMailBox(String mailBoxName, Message message) {

        boolean isMailBox = false;

        for (int i = 0; i < boxes.length; i++) {
            if (Objects.nonNull(boxes[i]) && Objects.equals(boxes[i].getName(), mailBoxName)) {
                isMailBox = true;
                break;
            }
        }

        if (!isMailBox) {//false
            System.out.println("MailBox with name " + mailBoxName + " not found");
            return;
        }

        for (MailBox mailBox : boxes) {
            if (Objects.nonNull(mailBox) && Objects.equals(mailBox.getName(), mailBoxName)) {
                message.setFrom(mailBox.getCreatedTemplate());
                mailBox.addMessage(message);
            }
        }
        System.out.println();
    }

    public void addMessageToMailBox1(String mailBoxName, String text, String from) {

        Message message = new Message(text, from);

        for (MailBox mailBox : boxes) {
            if (Objects.nonNull(mailBox)) {
                if (Objects.equals(mailBox.getName(), mailBoxName)) {
                    mailBox.addMessage(message);
                }
            }
        }
        System.out.println();
    }

    public void addMailBox(MailBox box) {
        for (int i = 0; i < boxes.length; i++) {

            if (boxes[i] != null && boxes[i].getName().equals(box.getName())) {
                NameMailBox nameMailBox = box.getNameMailBox();
                System.out.println("Dublicat " + box.getName() + nameMailBox.getName() + " mailBox");
                return;
            }

            if (boxes[i] == null) {
                boxes[i] = box;
                break;
            }
        }

    }

    //выводить в консоль список существующих адресов.
    public void showAllMailBox() {
        for (MailBox mailBox : boxes) {
            if (Objects.nonNull(mailBox)) {
                mailBox.showInfo();
            }
        }
    }

    public void showAllMessageInMailBox(String mailBox) {

        if (Objects.isNull(mailBox)) {
            System.out.println();
            return;
        }

        for (int i = 0; i < boxes.length; i++) {

            if (Objects.nonNull(boxes[i]) && boxes[i].getName().equals(mailBox)) {
                for (Message message : boxes[i].getMessages()) {
                    if (Objects.nonNull(message)) {
                        message.showInfo();
                    }
                }
            }

        }
    }

}

