package homeworks.post_server;

public enum NameMailBox {
    GMAIL("@gmail.com"),
    RAMBLER("@rambler.ru"),
    YANDEX("@yandex.ru");

    private String name;

    NameMailBox(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
