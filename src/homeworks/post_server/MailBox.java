package homeworks.post_server;

import java.util.Arrays;

public class MailBox {
    private String name;
    private Message[] messages;
    private NameMailBox nameMailBox;

    public MailBox(String name) {
        this.name = name;
        messages = new Message[10];
    }

    public MailBox(String name, NameMailBox nameMailBox) {
        this.name = name;
        this.messages = new Message[10];
        this.nameMailBox = nameMailBox;
    }

    public String getName() {
        return name;
    }

    public Message[] getMessages() {
        return messages;
    }

    public MailBox() {
        this.messages = new Message[10];
        fillMessages();
    }

    public NameMailBox getNameMailBox() {
        return nameMailBox;
    }

    private void fillMessages() {
        Message message1 = new Message("", "");
        Message message2 = new Message("", "");

        messages[0]= message1;
        messages[1]= message2;

    }

    public void addMessage(Message message) {
        for (int i = 0; i < messages.length; i++) {
            if (messages[i] == null) {
                messages[i] = message;
                break;
            }
        }
    }

    public void showInfo() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "MailBox{" +
                "name='" + name + nameMailBox.getName() +
                ", messages=" + Arrays.toString(messages) +
                '}';
    }

    public String getCreatedTemplate() {
        return name.concat(nameMailBox.getName());
    }
}
