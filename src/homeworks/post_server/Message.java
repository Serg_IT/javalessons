package homeworks.post_server;

public class Message {
    private String text;
    private String from;

    public Message(String text, String from) {
        this.text = text;
        this.from = from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getText() {
        return text;
    }

    public String getFrom() {
        return from;
    }

    public void showInfo() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "Message{" +
                ", text=" + text +
                ", from=" + from +
                '}';
    }
}
