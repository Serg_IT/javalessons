package homeworks;//27.10.2019//

public class DataTypesHW {
    public static void main(String[] args) {
        short s = 123;

        short s1 = 45;

        short s3 = (short) (s * s1);

        System.out.println("Result 123 * 45 = " + s3);

        float f = 23.5F;

        float f1 = 36.7F;

        float f3 = f - f1;
// task 1
        System.out.println("Result 23.5F - 36.7F = " + f3);

        byte b = 3, c = 4, d = 5;

        byte e = (byte) (b + c + d);

        System.out.println("Result 3 + 4 + 5 = " + e);

        short a1 = 5000, a2 = -2000, a3 = 7000;

        short a4 = (short) (a2 - a1 + a3);

        System.out.println ("Result - 2000 - 5000 + 7000 = " + a4);

        int i = 11, i1 = 22, i2 = 33, i3 = 44;

        int i4 = (int) (i * i1 * i2 + i3);

        System.out.println ("Result 11 * 22 * 33 + 44 = " +i4);

        int i5 = 44444, i6  = 55555, i7 =66666;

        int i8 = (int) (i6 / i5 * i7);

        System.out.println ("Result 55555 / 44444 * 66666 = " + i8);

        long l = 100_000_000_000_000_000L, l1 = 200_000_000_000_000_000L;

        long l2 = (long) (l1 - l);

        System.out.println("Result 200_000_000_000_000_000 - 100_000_000_000_000_000 = " +l2);

        float fl1 = 123.123F, fl2 = 321.321F, fl3 = 111.222F;

        float fl4 = (float) (fl1 * fl2 / fl3);

        System.out.println ("Result 123.123F * 321.321F / 111.222F = " + fl4);

        double d1 = 1.1, d2 = 22.22, d3 = 333.333;

        double d4 = (d2 / d1 * d);

        System.out.println ("Result 333.333 / 22.22 * 1.1 = " + d4);

        byte b1 = 127;

        short s4 = 32000;

        int i9 = 2_000_000;

        long l3 = 9_000_000_000_000_000_000L;

        float f4 = 1.777_777_7F;

        double d5 = 1.155_555_555_555_555;

        double d6 = (b1 + s4 + i9 + l3 +f4 +d5);

        System.out.println("Result 127 + 32000 + 2_000_000 + 9_000_000_000_000_000_000 + 1.777_777_7 + 1.155_555_555_555_555 = " +d6);

/* Task 2
        Binary
*78 (10) -> 01001110
78/2 = 39, ost 0
39/2 = 19, ost 1
19/2 = 9, ost 1
9/2 = 4, ost 1
4/2 = 2, ost 0
2/2= 1, ost 0
1
0
01001110 -> 78
0*2(7)+1*2(6)+0*2(5)+0*2(4)+1*2(3)+1*2(2)+1*2(1)+0*2(0) = 64 + 8 + 4 +2 = 78

*46 (10) -> 101110
46/2 = 23 ost 0
23/2 = 11 ost 1
11/2 = 5 ost 1
5/2 = 2 ost 1
2/2 = 1 ost 0
1
101110 -> 46
1*2(5)0*2(4)+1*2(3)+1*2(2)+0*2(1)+0*2(0) = 32 + 8 + 4 + 2 = 46

*23 (10) -> 10111
23/2 = 11 ost 1
11/2 = 5 ost 1
5/2 = 2 ost 1
2/2 = 1 ost 0
1
10111 -> 23
1*2(4)+0*2(3)+1*2(2)+1*2(1)+1*2(0)=16+4+2+1=23

* */
    }

}
