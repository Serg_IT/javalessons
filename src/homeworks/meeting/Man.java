package homeworks.meeting;

public class Man {
    private int age;
    private String gender;
    private String firstName;
    private String lastName;
    private String city;
    private int countOfChildren;

    public Man(int age,
               String gender,
               String firstName,
               String lastName,
               String city,
               int countOfChildren) {
        this.age = age;
        this.gender = gender;
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.countOfChildren = countOfChildren;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCity() {
        return city;
    }

    public int getCountOfChildren() {
        return countOfChildren;
    }

    @Override
    public String toString() {
        return "Man{" +
                "age=" + age +
                ", gender='" + gender + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lasttName='" + lastName + '\'' +
                ", city='" + city + '\'' +
                ", countOfChildren=" + countOfChildren +
                '}';
    }
}
