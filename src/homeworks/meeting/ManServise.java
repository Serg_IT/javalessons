package homeworks.meeting;

import java.util.Objects;

/*Создать приложение, позволяющие людям знакомиться.
Приложение должно позволять:
1) Регистрироваться человеку старше 18 лет.+
2) После регистрации выводить список подходящих мужчин/ женщин для этого человека по возрасту.+
3) Просматривать зарегистрировавшихся людей. Для мужчин выводить только женщин и наоборот.
4) Просматривать анкету отдельного человека(поиск по имени и фамилии)(совпадение)
5) Организовать "умный поиск". Пользователь вводит требования(город, пол, возраст, количество детей) и  выводить людей,
которые соответствуют требованиям.+
* */
public class ManServise {
    private Man[] men;

    public ManServise() {
        men = new Man[15];//null
        fillMen();
    }

    private void fillMen() {
        Man man1 =
                new Man(23, "Male", "Artur", "RIkov", "Riga", 1);
        Man man2 =
                new Man(25, "Male", "Oleg", "Foter", "Kiev", 2);
        Man man3 =
                new Man(23,
                        "Female",
                        "Irina",
                        "Dunlas",
                        "Riga",
                        1);
        Man man4 =
                new Man(23, "Female", "Vika", "Tivas", "Riga", 1);

        Man man5 =
                new Man(24, "Female", "Olga", "Rutova", "Tenav", 1);
        Man man6 =
                new Man(28, "Female", "Vika", "Benos", "Rekva", 0);
        Man man7 =
                new Man(23, "Male", "Farid", "Rikov", "Riga", 1);

        men[0] = man1;
        men[1] = man2;
        men[2] = man3;
        men[3] = man4;
        men[4] = man5;
        men[5] = man6;
        men[6] = man7;

    }

    public void addMan(Man m) {

        if (m.getAge() < 18) {
            System.out.println("You're yonger than 18 years old");
            return;
        }

        for (int i = 0; i < men.length; i++) {
            if (men[i] == null) {
                men[i] = m;
                break;
            }
        }

        showSuitableMen(m.getAge(), m.getGender());

    }

    private void showSuitableMen(int age, String gender) {
        for (int i = 0; i < men.length; i++) {
            if (men[i] != null && men[i].getAge() == age && !men[i].getGender().equals(gender)) {
                System.out.println("Suitable Men " + men[i]);
            }
        }
    }

    public void showAll() {
        for (Man m : men) {
            if (Objects.nonNull(m)) {
                System.out.println(m);
            }
        }
    }

    /*
     * Man m = new Man();
     * Man m = Man m;
     * */

    public void smartSearch(Man m) {/*поля каждого эл. массива сравниваем с полями входящего параметра, эл. массива
    и входящий параметер имеют одинаковый тип данных*/
        for (int i = 0; i < men.length; i++) {
            if (men[i] != null &&
                    !men[i].getGender().equals(m.getGender()) &&
                    men[i].getCity().equals(m.getCity()) &&
                    men[i].getCountOfChildren() == m.getCountOfChildren() &&
                    men[i].getAge() == m.getAge()) {
                System.out.println("SmartSearch " + men[i]);
            }

        }
    }

    public void showGenderMen(String gender) {
        for (int i = 0; i < men.length; i++) {
            if (men[i] != null && !men[i].getGender().equals(gender)) {
                System.out.println(men[i]);
            }
        }
    }

    public void showConsilienceMen(String firstName, String lastName) {
        for (int i = 0; i < men.length; i++) {
            if (men[i] != null && men[i].getFirstName().equals(firstName) &&
                    men[i].getLastName().equals(lastName)) {

                System.out.println("Consilience " + men[i]);
            }
        }

    }

}
