package homeworks.meeting;

public class TestMeeting {
    public static void main(String[] args) {
        ManServise manServise = new ManServise();

        Man man1 =
                new Man(23, "Male", "Niko", "Dasew", "Riga", 1);
        Man man3 =
                new Man(23,
                        "Female",
                        "Irina",
                        "Dunlas",
                        "Riga",
                        1);

        Man man2 =
                new Man(25, "Male", "Oleg", "Foter", "Kiev", 2);

        Man man4 =
                new Man(23, "Female", "Vika", "Tivas", "Riga", 1);

        Man man5 =
                new Man(24, "Female", "Olga", "Rutova", "Tenav", 1);
        Man man6 =
                new Man(28, "Female", "Vika", "Benos", "Rekva", 0);
        Man man7 =
                new Man(38, "Male", "Farid", "Rikov", "Bolts", 0);
        manServise.addMan(man3);

        manServise.smartSearch(man1);


//        manServise.showGenderMen(man1);
//        manServise.showGenderMen(man3);

        manServise.showConsilienceMen("Vika", "Tivas");

    }
}
