package homeworks.meeting;

import java.util.Scanner;

public class MenuMeeting {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {

        ManServise manServise = new ManServise();

        while (true) {
            menu();
            int key = SCANNER.nextInt();
            try {
                switch (key) {
                    case 1: {
                        System.out.println("Enter firstName");
                        String firstName = SCANNER.next();

                        System.out.println("Enter lastName");
                        String lastName = SCANNER.next();

                        System.out.println("Enter gender");
                        String gender = SCANNER.next();

                        System.out.println("Enter age");
                        int age = SCANNER.nextInt();

                        System.out.println("Enter city");
                        String city = SCANNER.next();

                        System.out.println("Enter count of children");
                        int countOfChildren = SCANNER.nextInt();

                        Man man = new Man(age, gender, firstName, lastName, city, countOfChildren);

                        manServise.addMan(man);
                        break;
                    }
                    case 2: {

                        System.out.println("Enter gender");
                        String gender = SCANNER.next();

                        System.out.println("Enter age");
                        int age = SCANNER.nextInt();

                        System.out.println("Enter city");
                        String city = SCANNER.next();

                        System.out.println("Enter count of children");
                        int countOfChildren = SCANNER.nextInt();

                        Man m = new Man(age, gender, null, null, city, countOfChildren);
                        manServise.smartSearch(m);

                        break;
                    }
                    case 3: {
                        manServise.showAll();

                        break;
                    }
                    case 4: {
                        System.out.println("Enter firstName");
                        String firstName = SCANNER.next();

                        System.out.println("Enter lastName");
                        String lastName = SCANNER.next();

                        manServise.showConsilienceMen(firstName, lastName);

                        break;
                    }
                    case 5: {
                        System.out.println("Enter gender");
                        String gender = SCANNER.next();

                        manServise.showGenderMen(gender);

                        break;
                    }

                    case 9:
                        return;
                    default:
                        return;
                }
            } catch (Exception e) {
                System.out.println("Exception" + e);
            }
        }
    }

    public static void menu() {
        System.out.println("\n1) Add man.\n" +
                "2) Smart search.\n" +
                "3) Show all men.\n" +
                "4) Show consilience men.\n" +
                "5) Show gender men.\n" +
                "6) ...\n" +
                "7) ...\n" +
                "8) ...\n" +
                "9) Exit.");
    }
}

