package homeworks.plant;

public class Employee {
    private boolean isManager;
    private int workDays;
    private String firstName;
    private String lastName;

    public Employee(boolean isManager, int workDays, String firstName, String lastName) {
        this.isManager = isManager;
        this.workDays = workDays;
        this.firstName = firstName;
        this.lastName = lastName;

    }

    public Employee() {
    }

    public int calculateSalary() {
        return workDays * (isManager ? 1000 : 100);
    }

    public String getFirstName() {
        return firstName;
    }

    public int getWorkDays() {
        return workDays;
    }

    public boolean isManager() {
        return isManager;
    }

    public void showInfo() {
//        System.out.println("Is manager " + isManager);
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "Employee{" +
                ", isManager=" + isManager +
                ", workDays=" + workDays +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
