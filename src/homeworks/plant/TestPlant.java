package homeworks.plant;

public class TestPlant {
    public static void main(String[] args) {
        PlantServise plantServise = new PlantServise();//departments are nulls

        Department secutiry = new Department("Security");
        Department it = new Department("IT");
        Department qwe = new Department("QWE");
        Department asd = new Department("ASD");
        Department zxc = new Department("ZXC");

        plantServise.addDepartment(secutiry);//departments[0] = secutiry, employees are nulls
        plantServise.addDepartment(it);//departments[1] = it, employees are nulls
        plantServise.addDepartment(qwe);
        plantServise.addDepartment(asd);
        plantServise.addDepartment(zxc);

        Employee employee = new Employee(true, 20, "Ivan", "Ivanov");
        Employee employee1 = new Employee(false, 2, "Kolya", "Petrov");
        Employee employee2 = new Employee(true, 12, "Ivan", "Rikes");//qwe
        Employee employee3 = new Employee(false, 20, "Fiona", "Bugko");
        Employee employee4 = new Employee(true, 17, "Fiona", "Shrek");//zxc
        Employee employee5 = new Employee(false, 22, "Farid", "Verit");

//        secutiry.add(employee);(is correct)

        plantServise.addEmployeeToDepartment(secutiry.getName(), employee);
        plantServise.addEmployeeToDepartment(it.getName(), employee1);
        plantServise.addEmployeeToDepartment(qwe.getName(),employee2);
        plantServise.addEmployeeToDepartment(asd.getName(),employee3);
        plantServise.addEmployeeToDepartment(zxc.getName(),employee4);
        plantServise.addEmployeeToDepartment(qwe.getName(),employee5);

//        plantServise.getDepartments();

//        plantServise.showManagersFromDifferentDepartments();
//        plantServise.showEmployeeInDepartmentAndSalary();

//        plantServise.showEmployeeInDepartmentWithDifferentWorkDays("ASD");
//        plantServise.showEmployeeFromDiffDepWithDifferentWorkDays("IT");
//        plantServise.showEmployeeAndSalary();
        plantServise.showEmployeeFromDiffDepWithSameFirstName("", "Fiona");


    }
}
