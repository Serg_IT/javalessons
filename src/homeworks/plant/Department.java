package homeworks.plant;

import java.util.Objects;

public class Department {
    private Employee[] employees;
    private String name;

    public Department(String nameDepartment) {
        this.name = nameDepartment;
        this.employees = new Employee[10];
    }

    public String getName() {
        return name;
    }

    public void add(Employee employee) {
        for (int i = 0; i < employees.length; i++) {
            if (Objects.isNull(employees[i])) {
                employees[i] = employee;
                break;
            }
        }

    }

    public Employee[] getEmployees() {
        return employees;
    }
}
