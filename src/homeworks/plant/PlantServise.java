/*
* 1)Создать базу данных сотрудников(управленцы и рядовые сотрудники).
Каждый сотрудник принадлежит своему отделу(отдел имеет название и номер) и имеет определенную зарплату.
Департамент может имеет одинаковый id, но разное название.
Зарплата сотруднику рассчитывается след. образом:
1) Для управленца количество раб.дней * 1000
2) Для сотрудника количество раб.дней * 100
По запросу выводить:
 - данные сотрудников, зарплата которых меньше 500 гривен+
 - данные управленцев из разных отделов(по названию)+
 - данные сотрудников, которые проработали больше 10, но меньше 20 раб. дней из одного -
 / из разных отделов(по названию)-
 - данные сотрудников, которые работают в одном отделе(по названию) и имеют зарплату > 700 гривен+
 - данные сотрудников, которые работают в разных отделах(по названию) и имеют одинаковые имена..

Управленцев и менеджеров хранить в одном массиве.
Данные об отделе хранить в отдельном массиве.
* */
package homeworks.plant;

import java.util.Objects;

public class PlantServise {
    private Department[] departments;

    public PlantServise() {
        departments = new Department[10];
    }

    public void addEmployeeToDepartment(String deptName, Employee employee) {
        for (Department department : departments) {//secutiry and IT
            if (Objects.nonNull(department)) {
                if (Objects.equals(department.getName(), deptName)) {//department = secutiry, deptName = IT
                    department.add(employee);
                }
            }
        }
    }

    public void addDepartment(Department department) {//
        for (int i = 0; i < departments.length; i++) {
            if (Objects.isNull(departments[i])) {
                departments[i] = department;//employees are nulls
                break;
            }
        }

    }

    //данные управленцев из разных отделов
    public void showManagersFromDifferentDepartments() {
        for (Department department : departments) {
            if (Objects.nonNull(department)) {
                for (Employee employee : department.getEmployees()) {
                    if (Objects.nonNull(employee) && employee.isManager()) {
                        employee.showInfo();
                    }
                }
            }
        }
    }


    public Department[] getDepartments() {
        return departments;
    }

    //  данные сотрудников, которые работают в одном отделе(по названию) и имеют зарплату > 700 гривень

    public void showEmployeeInDepartmentAndSalary() {
        for (Department department : departments) {
            if (Objects.nonNull(department)) {
                for (Employee employee : department.getEmployees()) {
                    if (Objects.nonNull(employee) && employee.calculateSalary() > 700) {
                        employee.showInfo();
                    }
                }
            }
        }
    }

    //данные сотрудников, зарплата которых меньше 500 гривен
    public void showEmployeeAndSalary() {
        for (Department department : departments) {
            if (Objects.nonNull(department)) {
                for (Employee employee : department.getEmployees()) {
                    if (Objects.nonNull(employee) && employee.calculateSalary() < 500) {
                        employee.showInfo();
                    }
                }
            }
        }
    }

    //данные сотрудников, которые проработали больше 10, но меньше 20 раб. дней из одного отдела

    public void showEmployeeInDepartmentWithDifferentWorkDays(String deptName) {
        for (Department department : departments) {
            if (Objects.nonNull(department) && department.getName().equals(deptName)) {
                for (Employee employee : department.getEmployees()) {
                    if (Objects.nonNull(employee) && employee.getWorkDays() > 10 && employee.getWorkDays() < 20) {
                        employee.showInfo();
                    }
                }
            }
        }
    }

    //данные сотрудников, которые проработали больше 10, но меньше 20 раб. дней  из разных отделов
    public void showEmployeeFromDiffDepWithDifferentWorkDays(String deptName) {
        for (Department department : departments) {
            if (Objects.nonNull(department) && !department.getName().equals(deptName)) {
                for (Employee employee : department.getEmployees()) {

//                    System.out.println("Show employees from " + department.getName() + " department");

                    if (Objects.nonNull(employee) && employee.getWorkDays() > 10 && employee.getWorkDays() < 20) {
                        employee.showInfo();
                    }
                }
            }
        }
    }
    //данные сотрудников, которые работают в разных отделах(по названию) и имеют одинаковые имена
    public void showEmployeeFromDiffDepWithSameFirstName(String deptName, String emplName) {
        for (Department department : departments) {
            if (Objects.nonNull(department) && !department.getName().equals(deptName)) {
                for (Employee employee : department.getEmployees()) {

                    if (Objects.nonNull(employee) && employee.getFirstName().equals(emplName)) {
                        System.out.println("Employees equals " + emplName);
                        employee.showInfo();
                    }

                }
            }
        }
    }
}
