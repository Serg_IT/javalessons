package homeworks;


import org.w3c.dom.ls.LSOutput;

import java.sql.SQLOutput;

public class CyclesHW {
    public static void main(String[] args) {
        //цикл for
//        for (int a = 100; a>10; a-=10){ //a-=,a--, a*/..
//            System.out.println(a);
//        for (int i = 1; i < 9; i++) {
//            System.out.println(i + " ");
//
//            //foreach
//            int[] array = {1, 2, 3, 4, 5};
//            for (int z : array) {
//                System.out.println(z + " ");
//
//
//            }
        //while (применяется boolean)
        boolean isTrue = true;
        int b = 0;
        while (isTrue) {//(b <= 10)
            System.out.println(b);
            b += 1;
            if (b > 7)
                isTrue = false;//break
        }
        //do..while (цикл хотя бы один раз выполнится, даже если условие не верно)
        int c = 1;
        do {
            c++;
            System.out.println(c);
        } while (c < 3);//c <= 0 - услове не верно, но один раз выполнится

    }
}

