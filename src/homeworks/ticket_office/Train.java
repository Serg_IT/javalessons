package homeworks.ticket_office;

/*
* Организовать приложение касса вокзала.
Приложение должно позволять:
1) Создавать рейс(количество вагонов, класс удобства,
город отбытия/прибытия, время в пути, скоростной/нескоростной).
2) Выводить список всех поездов.
3) Выводить список поездов по городу отбытия/прибытия.
4) Просматривать отдельный поезд.
5) Выводить поезда, у которых вагоны типа люкс/эконом.
* */
public class Train {
    private int numberTrain;
    private int countOfCarriage;
    private int categoryCarriage;
    private String departureCity;
    private String cityOfArrival;
    private String travelTime;
    private boolean isHighSpeed;

    public Train(int numberTrain, int countOfCarriage, int categoryCarriage, String departureCity, String cityOfArrival,
                 String travelTime, boolean isHighSpeed) {
        this.numberTrain = numberTrain;
        this.countOfCarriage = countOfCarriage;
        this.categoryCarriage = categoryCarriage;
        this.departureCity = departureCity;
        this.cityOfArrival = cityOfArrival;
        this.travelTime = travelTime;
        this.isHighSpeed = isHighSpeed;
    }

    public int getNumberTrain() {
        return numberTrain;
    }

    public int getCountOfCarriage() {
        return countOfCarriage;
    }

    public int getCategoryCarriage() {
        return categoryCarriage;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public String getCityOfArrival() {
        return cityOfArrival;
    }

    public String getTimeOfFlight() {
        return travelTime;
    }

    public boolean isHighSpeed() {
        return isHighSpeed;
    }

    public void show() {
        System.out.println(" " + numberTrain + " " + countOfCarriage + " " + categoryCarriage +
                " " + departureCity + " " + cityOfArrival + " " + travelTime + " " +
                isHighSpeed);
    }
}
