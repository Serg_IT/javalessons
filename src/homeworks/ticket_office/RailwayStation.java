package homeworks.ticket_office;

/*
* Организовать приложение касса вокзала.
Приложение должно позволять:
1) Создавать рейс(количество вагонов, класс удобства,
город отбытия/прибытия, время в пути, скоростной/нескоростной).+
2) Выводить список всех поездов.+
3) Выводить список поездов по городу отбытия/прибытия.+
4) Просматривать отдельный поезд.+
5) Выводить поезда, у которых вагоны типа люкс/эконом.+
* */
public class RailwayStation {

    private Train[] trains;

    public RailwayStation() {
        trains = new Train[12];
        fillTrain();
    }

    private void fillTrain() {
        Train train1 = new Train(41, 10, 1, "Dnipro",
                "Lviv", "17:00", true);
        Train train2 = new Train(325, 15, 1, "Odessa",
                "Moskva", "36:00", true);
        Train train3 = new Train(345, 17, 2, "KriviyRig",
                "Kharkiv", "15:00", true);
        Train train4 = new Train(721, 16, 1, "Kyiv",
                "Minsk", "18:00", true);
        Train train5 = new Train(564, 20, 2, "Kyiv",
                "Berdyansk", "12:00", false);
        Train train6 = new Train(205, 11, 1, "Nikolaiv",
                "Zaporizhia", "8:00", true);
        Train train7 = new Train(567, 14, 2, "Херсон",
                "Жмеринка", "28:00", false);
        Train train8 = new Train(78, 19, 2, "Мариуполь",
                "Кропивницкий", "14:00", true);
          Train train9 = new Train(45, 21, 1, "Адлер",
                "Львов", "24:00", true);
          Train train10 = new Train(656, 22, 1, "Брянск",
                "Вена", "24:00", true);

        trains[0] = train1;
        trains[1] = train2;
        trains[2] = train3;
        trains[3] = train4;
        trains[4] = train5;
        trains[5] = train6;
        trains[6] = train7;
        trains[7] = train8;
        trains[8] = train9;
        trains[9] = train10;

    }
//Выводить список всех поездов
    public void addTrain(Train tr) {
        for (int i = 0; i < trains.length; i++) {
            if (trains[i] != null && trains[i].getNumberTrain() == tr.getNumberTrain()) {
                System.out.println("Dublicat " + tr.getNumberTrain() + "number's train");
                return;
            }

            if (trains[i] == null) {
                trains[i] = tr;
            }
        }
    }

    public void showAllTrains() {
        System.out.println("All trains -> ");
        for (int i = 0; i < trains.length; i++) {
            if (trains[i] != null) {
                trains[i].show();
            }
        }
    }
//Просматривать отдельный поезд
    public void showSearchTrain(int number) {
        System.out.println("Search train -> ");

        for (int i = 0; i < trains.length; i++) {
            if (trains[i] != null && trains[i].getNumberTrain() == number) {
                trains[i].show();
            }
        }
    }
//Выводить поезда, у которых вагоны типа люкс/эконом.
    public void showCategoryCarriageTrain(int number) {
        System.out.println("Category carriage " + number + " of traine " + " -> ");

        for (int i = 0; i < trains.length; i++) {
            if (trains[i] != null && trains[i].getCategoryCarriage() == number) {
                trains[i].show();
            }
        }
    }

    //Выводить список поездов по городу отбытия/прибытия
    public void showSuitableTraine(String departureCity, String cityOfArrival) {
        System.out.println("Suitable train -> ");

        for (int i = 0; i < trains.length; i++) {
            if (trains[i] != null && trains[i].getDepartureCity().equals(departureCity) ||
                    trains[i].getCityOfArrival().equals(cityOfArrival)) {
                trains[i].show();
            }
        }

    }
}
