package homeworks.work_class;
//Создать класс Group c полями название, кол-во студентов
public class Group {
    private int numberGroup;
    private int numberOfStudents;

    public Group (int number) {
        this.numberGroup = number;
    }

    public int getNumberGroup() {
        return numberGroup;
    }

    public void setNumberGroup(int numberGroup) {
        this.numberGroup = numberGroup;
    }
}
