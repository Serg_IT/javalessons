/*Создать класс Student c полями имя, пол, фамилия, возраст, Group(отдельный класс), University(отдельный класс).
Создать класс Group c полями название, кол-во студентов.
Создать класс University c полями название, кол-во групп, дата основания.

Создать несколько студентов:
- с одинаковой группой и университетом.
- с разными группами в университете.
Создать методы позволяющие:
 - вывести данные о студенте с учетом данных об университете и группе.
 - проверять группы на одинаковость у двух студентов.
 - проверять университеты на одинаковость у двух студентов.
 - проверять однофамильцев у двух студентов.
*/


package homeworks.work_class;

public class Student {
    private String firstName;
    private String gender;
    private String lastName;
    private int age;
    private Group group;
    private University university;

    public Student(String firstName, String gender, String lastName,
                   int age, Group group, University university) {
        this.firstName = firstName;
        this.gender = gender;
        this.lastName = lastName;
        this.age = age;
        this.group = group;
        this.university = university;
    }

    public Group getGroup() {
        return group;
    }

    public String getFirstName() {
        return firstName;
    }

    public University getUniversity() {
        return university;
    }

    public void compareGroups(Student st) {
        int gpOne = st.getGroup().getNumberGroup();

        int gpThis = this.getGroup().getNumberGroup();

        if (gpOne == gpThis) {
            System.out.println("Positions are equal");
            return;
        }

        System.out.println("Positions aren't equal");
    }

    public void compareUniversity(Student st) {
        University unOne = st.getUniversity();
        University unTwo = this.getUniversity();

        String nameOne = unOne.getNameUniversity();
        String nameTwo = unTwo.getNameUniversity();

        if (nameOne.equals(nameTwo)) {
            System.out.println("Universities are equal");
            return;
        }

        System.out.println("Universities aren't equal");
    }
}
