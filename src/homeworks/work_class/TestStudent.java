package homeworks.work_class;

public class TestStudent {
    /*Создать несколько студентов:
- с одинаковой группой и университетом.
- с разными группами в университете.
Создать методы позволяющие:
 - вывести данные о студенте с учетом данных об университете и группе.
 - проверять группы на одинаковость у двух студентов.
 - проверять университеты на одинаковость у двух студентов.
 - проверять однофамильцев у двух студентов.*/

    public static void main(String[] args) {
        University ddu = new University("DDU");
        University mau = new University("MAU");
        University npu = new University("NPU");

//        ddu.compareUniversities1(mau);

        Group group1 = new Group(1);
        Group group2 = new Group(2);
        Group group3 = new Group(3);

        Student student =
                new Student("Vasil", "Male", "Reval", 19, group1, ddu);
        Student student1 =
                new Student("Kiril", "Male", "Vitov", 20, group1, ddu);
        Student student2 =
                new Student("Viktor", "Male", "Patov", 18, group2, mau);
        Student student3 =
                new Student("Olga", "Female", "Osipova", 18, group3, mau);

//        group1.setNumberGroup(10);

//        System.out.println(student.getGroup().getNumberGroup());

        student1.compareGroups(student2);

        student1.compareUniversity(student2);

    }
}
