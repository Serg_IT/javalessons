package homeworks.work_class.department;

public class Position {
    private String positionName;

    public Position(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionName() {
        return positionName;
    }
}
