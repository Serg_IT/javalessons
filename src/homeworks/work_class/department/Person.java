package homeworks.work_class.department;

/*
*Создать класс Person c полями имя, пол, фамилия, возраст, должность.
Для должности создать отдельный класс с полем название должности.

Создать несколько людей с одинаковой и разными должностями.
Создать методы позволяющие:
 - вывести данные о человеке.
 - проверять должности на одинаковость у двух людей.
 - проверять однофамильцев у двух людей.*
* */
public class Person {
    private String firstName;
    private String gender;
    private String lastName;//"Ken"
    private int age;
    private Position position;

    public Person(String firstName, String gender, String lastName,
                  int age, Position position) {
        this.firstName = firstName;
        this.gender = gender;
        this.lastName = lastName;
        this.age = age;
        this.position = position;
    }

    public void showInformation() {
        System.out.println(firstName + "\t" + lastName + "\t" + gender + "\t" + age + "\t" + position.getPositionName());
    }

    public Position getPosition() {
        return position;
    }

    public String getLastName() {
        return lastName;
    }

    public void comparePositions(Person one) {
        Position positionOne = one.getPosition();

        Position positionTwo = this.getPosition();

        if (positionOne.getPositionName().equals(positionTwo.getPositionName())) { //equals
            System.out.println("Positions are equal");
            return;
        }

        System.out.println("Positions aren't equal");
    }

    public void compareLastName(Person person) {
        if (this.lastName.equals(person.getLastName())) {
            System.out.println("LastNames are equal");
            return;
        }

        System.out.println("LastNames aren't equal");
    }
}
