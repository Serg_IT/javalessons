package homeworks.work_class.department;

public class TestPerson {
    public static void main(String[] args) {
        Position programmer = new Position("Programmer");
        Position manager = new Position("Manager");
        Position administrator = new Position("Administrator");

        Person personProg = new Person("Kan", "Male", "One", 33, programmer);
        Person personProg1 = new Person("Kile", "Male", "One", 25, programmer);
        Person personManager = new Person("Fiona", "Female", "Dervy", 24, manager);
        Person personAdministrator =
                new Person("Robin", "Male", "Vorsy", 29, administrator);

        personAdministrator.showInformation();

        personProg.comparePositions(personProg1);
        personProg.comparePositions(personManager);

        personProg1.compareLastName(personProg);
        personAdministrator.compareLastName(personManager);

    }
}
