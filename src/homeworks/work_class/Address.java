package homeworks.work_class;
/*Создать класс Адрес(индекс города, кол-во жителей, кол-во улиц) и несколько объектов
 Сделать:
 - увеличить кол-во жителей на 20% в городах, где индекс больше 50000
 - вывести кол-во улиц, где индекс меньше 50000*/

public class Address {

    private int index;
    private float population;
    private int numberOfStreets;

    Address(int cIndex, float cPopulation, int cStreets) {

        if (cIndex > 50000) {
            cPopulation *= 1.2F;
        }

        population = cPopulation;

        System.out.println(cPopulation);

        index = cIndex;
        numberOfStreets = cStreets;

        if (cIndex < 50000) {
            System.out.println(cStreets);
        }
    }




}

class NewAddress {

    private int index;
    private float population;
    private int numberOfStreets;

    NewAddress(int cIndex, float cPopulation, int cStreets) {
       population = cPopulation;
       index = cIndex;
       numberOfStreets = cStreets;

    }

    public void setPopulation(float population) {
        this.population = population;
    }

    public float getPopulation() {
        return population;
    }

    public void showCountStreets() {
        if (index < 50000) {
            System.out.println(numberOfStreets);
        }
    }
}

class TestAddress {
    public static void main(String[] args) {
        Address Dnepr = new Address(49000, 1500000, 500);
        Address Kamenskoe = new Address(51909, 300000, 200);
        Address KrivoyRog = new Address(50479, 300000, 200);

    }


}class NewTestAddress {
    public static void main(String[] args) {
        NewAddress dnepr = new NewAddress(49000, 1500000, 500);
        NewAddress kamenskoe = new NewAddress(51909, 300000, 200);
        NewAddress krivoyRog = new NewAddress(50479, 300000, 200);

        NewAddress[] arr = new NewAddress[3];
        arr[0] = dnepr;
        arr[1] = kamenskoe;
        arr[2] = krivoyRog;

        for (NewAddress address : arr) {
            float pop = address.getPopulation();
            address.setPopulation(pop *= 1.2F);

//            System.out.println(pop);

            address.showCountStreets();
        }

    }


}