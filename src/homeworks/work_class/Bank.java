package homeworks.work_class;

/*Создать класс Банковский счет.
        Установить изначально сумму для счета - 200$
        Создать метод, который будет отнимать от счета сумму.
        Если денег недостаточно, тогда написать в консоль - "Пополните счет".
        Счет можно пополнить другим методом.*/
public class Bank {
    private int sum;

    public Bank() {
        sum = 200;
    }

    public void withDrawal(int credit) {//100
        if (sum < credit) {
            System.out.println("Add money");
        } else {
            sum -= credit;
        }
    }

    public void addMoney(int cash) {
        sum += cash;
    }

    public int getSum() {
        return sum;
    }

    public void printSum() {
        System.out.println(sum);
    }

}

class TestBank {
    public static void main(String[] args) {
        Bank privat = new Bank();

        privat.withDrawal(100);

//        System.out.println(privat.getSum());
        privat.printSum();
        privat.addMoney(500);

//        System.out.println(privat.getSum());
        privat.printSum();

    }
}
