package homeworks.work_class;

/*1) Write Computer class, the attributes of this class is
serialNumber (of int type), price (of float type),
quantityCPU (of int type) and frequencyCPU (of int type).

2) The fields Computer class need to be encapsulated.
 Add to Computer class getters and setters methods. Use correct access modifiers.
3) Write a program to create array of Computer objects (5 pcs.).
Declare array of Computer objects (5 pcs.), create 5 Computer objects and place it to array.
4) Write a program that iterate through array of Computer objects and increases by 10 percent field price.
5) Add to class Computer method void view(){} that prints all fields of object in line.
Print all info (fields) of all objects in console.*/
public class Computer {
    private int serialNumber;
    private int quantityCPU;
    private int frequencyCPU;
    private float price;

    public Computer(int cFrequencyCPU, int cQuantityCPU, int cSerialNumber, float cPrice) {
        frequencyCPU = cFrequencyCPU;
        quantityCPU = cQuantityCPU;
        serialNumber = cSerialNumber;
        price = cPrice;
    }

    public void setFrequencyCPU(int cFrequencyCPU) {
        frequencyCPU = cFrequencyCPU;
    }

    public int getFrequencyCPU() {
        return frequencyCPU;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void view() {
        System.out.print("\t " + frequencyCPU + " " + quantityCPU + " " + serialNumber + " " + price);

    }
}

class TestComputer {
    public static void main(String[] args) {
        int[] ints = new int[10];//0

        Computer[] computers = new Computer[5];//nulls

        Computer computer1 = new Computer(3, 2, 12, 15.5F);
        Computer computer2 = new Computer(5, 1, 10, 14.2F);
        Computer computer3 = new Computer(4, 5, 11, 19.3F);
        Computer computer4 = new Computer(6, 7, 18, 23.4F);
        Computer computer5 = new Computer(7, 8, 22, 43.1F);
        /*Computer computer3 = null;
        computer3 = new Computer(5, 1, 12, 14.2F);*/

        computers[0] = computer1;
        computers[1] = computer2;
        computers[2] = computer3;
        computers[3] = computer4;
        computers[4] = computer5;

        for (int i = 0; i < computers.length; i++) {//try to use foreach
            Computer computer = computers[i];

            float price = computer.getPrice();

            float changedPrice = price * 1.1F;

            computer.setPrice(price * 1.1F);

            System.out.print("Computer " + (i + 1));

            computer.view();

            System.out.println();

        }

    }

}

