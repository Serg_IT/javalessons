package homeworks.airport;

public class Flight {
    private int numberFlight;
    private String departureСountry;
    private String countryOfArrival;
    private String departureCity;
    private String cityOfArrival;
    private String timeOfFlight;
    private int numberOfSeats;

    public Flight(int numberFlight,
                       String departureСountry,
                       String countryOfArrival,
                       String departureCity,
                       String cityOfArrival,
                       String timeOfFlight,
                       int numberOfSeats) {
        this.numberFlight = numberFlight;
        this.departureСountry = departureСountry;
        this.countryOfArrival = countryOfArrival;
        this.departureCity = departureCity;
        this.cityOfArrival = cityOfArrival;
        this.timeOfFlight = timeOfFlight;
        this.numberOfSeats = numberOfSeats;
    }

    public int getNumberFlight() {
        return numberFlight;
    }

    public String getDepartureСountry() {
        return departureСountry;
    }

    public String getCountryOfArrival() {
        return countryOfArrival;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public String getCityOfArrival() {
        return cityOfArrival;
    }

    public String getTimeOfFlight() {
        return timeOfFlight;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    //show sout all fields
    public void show() {
        System.out.println(" " + numberFlight + " " + departureСountry + " " + countryOfArrival + " " + departureCity +
                " " + cityOfArrival + " " + timeOfFlight + " " + numberOfSeats);

    }
}
