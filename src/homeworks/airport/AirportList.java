package homeworks.airport;

/*Создать приложение, позволяющие создавать расписание аэропорта.
Приложение должно позволять:
1) Создавать рейс.+
2) После ввода страны прибытия и/или страны отправления выводить список подходящих рейсов.
3) Просматривать все рейсы.
4) Просматривать информацию о конкретном рейсе.
5) Организовать "умный поиск". Пользователь вводит требования(город отправления, город прибытия, время в полете, количество мест)
и выводить рейс, который соответствует требованиям.*/
public class AirportList {

    private Flight[] flights;

    public AirportList() {
        this.flights = new Flight[10];
        fillFlight();
    }

    private void fillFlight() {
       /* Flight flight1 = new Flight(123321, "Ua", "Ru",
                "Kiev", "Moscow", "2 hours", 30);
        Flight flight2 = new Flight(111222, "Ua", "De",
                "Kiev", "Berlin", "3 hours", 30);
        Flight flight3 = new Flight(222333, "Switz", "Cz",
                "Berne", "Praga", "2 hours", 60);
        Flight flight4 = new Flight(333444, "De", "Sk",
                "Berlin", "Bratislava", "2 hours", 60);
        Flight flight5 = new Flight(555666, "Cz", "Esp",
                "Praga", "Madrid", "4 hours", 60);
        Flight flight6 = new Flight(666777, "Esp", "Pol",
                "Madrid", "Warsaw", "2 hours", 60);
        Flight flight7 = new Flight(888999, "Ua", "Switz",
                "Kiev", "Berne", "4 hours", 60);

        flights[0] = flight1;
        flights[1] = flight2;
        flights[2] = flight3;
        flights[3] = flight4;
        flights[4] = flight5;
        flights[5] = flight6;
        flights[6] = flight7;*/


    }

    public void addFlight(Flight fl) {

        for (int i = 0; i < flights.length; i++) {

            if (flights[i] != null && flights[i].getNumberFlight() == fl.getNumberFlight()) {
                System.out.println("Duplicat " + fl.getNumberFlight() + " number's flight");
                return;
            }

            if (flights[i] == null) {
                flights[i] = fl;
                break;
            }

        }

//        showSuitableFlight(fl.getDepartureСountry(), fl.getCountryOfArrival());
    }

    public void showSuitableFlight(String departureСountry, String countryOfArrival) {

        System.out.println("Suitable flights -> ");

        for (int i = 0; i < flights.length; i++) {
            if (flights[i] != null && (flights[i].getDepartureСountry().equals(departureСountry) ||
                    flights[i].getCountryOfArrival().equals(countryOfArrival))) {
                flights[i].show();
            }
        }
    }

    /*Организовать "умный поиск". Пользователь вводит требования
    (город отправления, город прибытия, время в полете, количество мест)
    и выводить рейс, который соответствует требованиям*/
    public void smartSearch(Flight fl) {
        System.out.println("SmartSearch -> ");

        for (int i = 0; i < flights.length; i++) {
            if (flights[i] != null &&
                    flights[i].getDepartureCity().equals(fl.getDepartureCity()) &&
                    flights[i].getCityOfArrival().equals(fl.getCityOfArrival()) &&
                    flights[i].getTimeOfFlight().equals(fl.getTimeOfFlight()) &&
                    flights[i].getNumberOfSeats() == fl.getNumberOfSeats()) {
                flights[i].show();
            }
        }
    }

    public void showAllFlights() {
        System.out.println("All flights -> ");
        for (int i = 0; i < flights.length; i++) {
            if (flights[i] != null) {
//                System.out.println(flights[i] + " ");
                flights[i].show();
            }

        }
    }

    public void showSearchFlight(int number) {

        System.out.println("Search flight -> ");

        for (int i = 0; i < flights.length; i++) {
            if (flights[i] != null && flights[i].getNumberFlight() == number)
                flights[i].show();
        }
    }

}
