package homeworks.airport;

/*Создать приложение, позволяющие создавать расписание аэропорта.
Приложение должно позволять:
1) Создавать рейс.
2) После ввода страны прибытия и/или страны отправления выводить список подходящих рейсов.
3) Просматривать все рейсы.
4) Просматривать информацию о конкретном рейсе.
5) Организовать "умный поиск". Пользователь вводит требования(город отправления, город прибытия, время в полете, количество мест)
и выводить рейс, который соответствует требованиям.*/
public class TestAirport {
    public static void main(String[] args) {
        AirportList airportList = new AirportList();

        Flight flight1 = new Flight(123321, "Ua", "Ru",
                "Kiev", "Moscow", "2 hours", 30);

        Flight flight2 = new Flight(1112220, "Ua", "De",
                "Kiev", "Berlin", "3 hours", 30);
        Flight flight3 = new Flight(222333, "Switz", "Cz",
                "Berne", "Praga", "2 hours", 60);
        Flight flight4 = new Flight(333444, "De", "Sk",
                "Berlin", "Bratislava", "2 hours", 60);
        Flight flight5 = new Flight(555666, "Cz", "Esp",
                "Praga", "Madrid", "4 hours", 60);
        Flight flight6 = new Flight(666777, "Esp", "Pol",
                "Madrid", "Warsaw", "2 hours", 60);
        Flight flight7 = new Flight(888999, "Ua", "Switz",
                "Kiev", "Berne", "4 hours", 60);


        airportList.addFlight(flight1);
        airportList.addFlight(flight2);
        airportList.addFlight(flight3);
        airportList.addFlight(flight4);
        airportList.addFlight(flight5);
        airportList.addFlight(flight6);
        airportList.addFlight(flight7);

        airportList.showSuitableFlight("Esp", "Esp");

//        airportList.smartSearch(flight1);

        airportList.showAllFlights();

//        airportList.showSearchFlight(123321);
    }

}
