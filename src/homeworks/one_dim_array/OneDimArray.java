package homeworks.one_dim_array;

import java.util.Random;

public class OneDimArray {
    public static void main(String[] args) {
        Random random = new Random();

        int[] array = new int[25];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(30);
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        System.out.println();

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                System.out.print(array[i] + " ");
            }
        }
    }
}

/*
* Создать два массива из 20 чисел. Первый массив проинициализировать четными числами.
Проинициализировать второй массив элементами первого массива при условии,
что индекс делится на 4 без остатка и элемент больше 3, но меньше 16.
Если условие не выполняется оставить элемент массива без изменения.
* */

class HW {
    public static void main(String[] args) {
        int length = 20;
        int[] array1 = new int[length];
        int[] array2 = new int[length];

        for (int i = 0; i < array1.length; i++) {
            array1[i] = i * 2;// создание нечетных + 1

            System.out.print(array1[i] + "\t");

            if (i % 4 == 0 && array1[i] > 3 && array1[i] < 16) {
                array2[i] = array1[i];
            }
        }

        System.out.println();

        for (int i = 0; i < array2.length; i++) {
            System.out.print(array2[i] + "\t");
        }
    }


}

/*
*
* There are statistics for the year by months as an array:
               int[] m = new int[] { 10, 21, 5, 22, 9, 29, 25, 22, 11, 14, 8, 14 };
Write code which calculates the maximum value from the array, the minimum value and the average.*/
class HW1 {
    public static void main(String[] args) {
        int[] array = new int[]{10, 21, 5, 22, 9, 29, 25, 22, 11, 14, 8, 14};

        int r = 4;

        int max = array[2];//5

        int min = array[1];//5

        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            int element = array[i];

            sum += element;

            if (element > max) {//10 > 5
                max = element;
            }

            if (element < min) {//10 > 5
                min = element;
            }
        }

        System.out.println("Max = " + max + "\tMin = " + min + "\tAvg = " + sum * 1.0 / array.length);//5 / 2 = 2

    }

}

/*
 *
 * В двумерном массиве целых чисел определить, сколько раз в нем встречается элемент со значением X.*/

class TwoDimArray {
    public static void main(String[] args) {

        Random random = new Random();

        int[][] arr = {
                {5, 4, 8},
                {1, 2, 9}
        };

//        arr[][]

        int[][] array = new int[4][4];

        int count = 0;

        int value = 23;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                array[i][j] = random.nextInt(50);

                System.out.print(array[i][j] + "\t");

                if (array[i][j] == value) {
                    count++;
                }
            }

            System.out.println();

        }

        System.out.println("Count = " + count);
    }
}
/*
 *
 * В двумерном массиве Вывести максимальный/min элемент каждой строки
 * */

class TwoDimArray1 {
    public static void main(String[] args) {

        Random random = new Random();

        int[][] array = new int[4][4];

        int[][] arr = {
                {5, 4, 8},// 5 max = 8, min = 4 [0][0]
                {1, 2, 9}//1 max = 1, min = 1 [1][0]
        };

        int[][] arr1 = {
                {5, 4, 8},// 5 max = 8, min = 4 [0][0]
                {1, 9},//1 max = 1, min = 1 [1][0]
                {1}//1 max = 1, min = 1 [1][0]
        };


        for (int i = 0; i < array.length; i++) {

            for (int j = 0; j < array[0].length; j++) {
                array[i][j] = random.nextInt(50);//(max - min + 1) + min; (1 + 1 + 1 = 3) - 1

                System.out.print(array[i][j] + "\t");
            }

            System.out.println();
        }


        /*for (int i = 0; i < array.length; i++) {//0

            int max = array[i][0];
            int min = array[i][0];

            System.out.println("Row = " + i + "\tMax = " + max + "\tMin = " + min);

            for (int j = 0; j < array[0].length; j++) {
                if (array[i][j] > max) {
                    max = array[i][j];
                }

                if (array[i][j] < min) {
                    min = array[i][j];
                }
            }


        }*/

        int max = array[0][0];//49
        int min = array[0][0];//1

        for (int i = 0; i < array.length; i++) {//0

            max = array[i][0];//0 1 2 3
            min = array[i][0];

            for (int j = 0; j < array[0].length; j++) {
                if (array[i][j] > max) {
                    max = array[i][j];
                }

                if (array[i][j] < min) {
                    min = array[i][j];
                }
            }

            System.out.println("Row = " + i + "\tMax = " + max + "\tMin = " + min);


        }

    }
}

/*
* 1 5 6 4
* 1 2 3 4
*
*
* */
