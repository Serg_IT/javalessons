package homeworks.one_dim_array;

public class OneDimensionArraysHW {
    public static void main(String[] args) {
/*
        int b = 2;

        //data_type[] name_array = new data_type[size];

        int  [] arr = new int [2];
        arr [0] = 12;
        arr [1] = 13;

        System.out.println(arr[0]);
        System.out.println(arr [1]);
*/

/*
        int [] nums = new int [3];

        nums [0] = 101;
        nums [1] = 102;
        nums [2] = 103;


        System.out.println(nums [0]);
        System.out.println(nums [1]);
        System.out.println(nums [2]);
*/

        int[] nums = {123, 1234, 12345, 123456, 1234567};
//        nums [0] = 321;
//        nums [1] = 4321; и тд
//        System.out.println(nums [0]);

        /*int [] nums = new int [5];
        nums [0] = 1;
        nums [1] = 2;
        nums [2] = 3;
        nums [3] = 4;
        nums [4] = 5;*/

        System.out.println(nums[0] + "\t" + nums[1]);
        System.out.println(nums[1]);
        System.out.println(nums[2]);
        System.out.println(nums[3]);
        System.out.println(nums[4]);// *можно ли вывести все элементы массива в консоль в ряд?

        for (int i = 0; i < nums.length; i++) {

            System.out.print(nums[i] + "\t");

        }


    }
}
