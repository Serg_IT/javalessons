package homeworks.one_dim_array;

public class Fibonnachi {
    public static void main(String[] args) {
//        0 1 2 3 4 5 6  7
//        1 1 2 3 5 8 13 21 34

        int[] array = new int[10];

        array[0] = 1;
        array[1] = 1;

        for (int i = 2; i < array.length; i++) {
            array[i] = array[i - 1] + array[i - 2];
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }



    }
}
