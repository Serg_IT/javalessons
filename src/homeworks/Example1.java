package homeworks;//27.10.2019, арифм.операторы, инкремент, декремент//

public class Example1 {
    public static void main(String[] args) {

        int n = 11, n1 = 12, n2 = 13;
        n -= 1;
        n1 /= 2;
        n2 *= 2;
        int n3 = n1 - n;
        int n4 = n2 * 2;
        int m = 280;
        m /= 2;

        System.out.println("Result 11 - 1 = " + n);
        System.out.println("Result 12 / 2 = " + n1);
        System.out.println("Result 13 * 2 = " + n2);
        System.out.println("Result 12 - 11 = " + n3); //новая n = 10, новая n1 =6//
        System.out.println("Result 13 * 2 = " + n4); // новая n2 = 26//
        System.out.println("Result 280 / 2 = " + m);

        int size = 35;
        size++; //инкремент//

        System.out.println("Result 35 + 1 = " + size);//увел-е переменной на 1//

        int num = 123;
        num--; //декремент//

        System.out.println("Result 123 - 1 =" + num); //умень-е пер. на 1//

        int a = 7, b = 8;
        int c = 7 * a++;
        int d = 7 * ++b;

        System.out.println("Result c = 7 * 7 = " + c); //С = 49, А = 8 (постфикс)//
        System.out.println("Result d = 7 * (8+1) = " + d);//D = 63, B = 9 (префикс)//

        int e = 2, f = 3;
        int g = 6 / f--;
        int h = 3 / --e;

        System.out.println(g);//g = 2,теперь f = 2//
        System.out.println(h);//h = 3, теперь e = 1//

        int k = 10;
        int l = k + f + e;

        System.out.print("Result 10 + 2 + 1 = " + l);

    }


}
