package homeworks.two_dim_array;

import java.util.Random;

public class HW07_12_19 {
    public static void main(String[] args) {
/*
        В двумерном массиве Необходимо найти и вывести наибольший, наименьший элемент,
        avg всех элементов каждого столбца.
*/
        Random random = new Random();

        int[][] array = new int[4][4];

        for (int i = 0; i < array.length; i++) {

            for (int j = 0; j < array.length; j++) {
                array[i][j] = random.nextInt(5);

                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }

        int max = array[0][0];
        int min = array[0][0];
        int sumColum = 0;

        for (int i = 0; i < array.length; i++) {

            max = array[i][0];
            min = array[i][0];
            sumColum = 0;

            for (int j = 0; j < array.length; j++) {


                if (array[j][i] > max) {
                    max = array[j][i];
                }
                if (array[j][i] < min) {
                    min = array[j][i];
                }
                sumColum += array[j][i];
            }
            System.out.println("Colum = " + i + "\tMax = " + max + "\tMin = " + min + "\tSum = " + sumColum + "\tAvg = " + sumColum * 1.0 / array.length);

        }
    }
}

