package homeworks.two_dim_array;

import java.util.Random;

/*В двумерном массиве вывести индекс первого встретившегося элемента, число дано*/
public class FoundValue {
    public static void main(String[] args) {
        Random random = new Random();

        int[][] array = {
                {26, 65, 77, 91},
                {75, 13, 96, 99},
                {55, 24, 73, 92},
                {40, 17, 22, 17}
        };

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
//                array[i][j] = random.nextInt(90) + 10;
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }

        int indexRow = -1;
        int indexColumn = -1;

        int foundValue = 73;

        for (int i = 0; i < array.length; i++) {// break, continue
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] == foundValue) {
                    indexRow = i;
                    indexColumn = j;
                    break;
                }
            }

            if (indexColumn != -1 && indexRow != -1) {
                System.out.println(indexRow + ";" + indexColumn);
                break;
            }

        }

    }
}
