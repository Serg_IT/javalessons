package homeworks.two_dim_array;

import java.util.Random;

//Создать два массива из 30 чисел. Первый массив проинициализировать нечетными числами.
//Проинициализировать второй массив элементами первого массива при условии,
//что индекс элемента больше 4 и делится без остатка на 5 и элемент больше 0, но меньше 6 или больше 10, но меньше 20.
//Если условие не выполняется оставить элемент массива без изменения.
public class HW16_11_19 {
    public static void main(String[] args) {

        int length = 30;
        int[] array1 = new int[length];
        int[] array2 = new int[length];
        for (int i = 0; i < array1.length; i++) {

            array1[i] = i * 2 + 1;

            System.out.print(array1[i] + "\t");
//                  false                     &&                false              || true -> false || true -> true
            // true  &&   false              &&    true         &&   false         ||   true           &&    true
//            if (i > 4 && i % 5 == 0 && array1[i] > 0 && array1[i] < 6 || array1[i] > 10 && array1[i] < 20) {
            if ((i > 4 && i % 5 == 0) && ((array1[i] > 0 && array1[i] < 6) || (array1[i] > 10 && array1[i] < 20))) {
                array2[i] = array1[i];
            }
//            true && false && ((true && false) || (true && true))
//            true && false && ((false) || (true)) -> true && false && true -> false
        }

        System.out.println();

        for (int i = 0; i < array2.length; i++) {
            System.out.print(array2[i] + "\t");
        }
    }
}

/*
i == 4
without ()        false &&   false   &&    true       && false       || false          &&   true -> false && false||false
with ()        false &&   false   &&    ((true       && false)       || (false          &&   true)) -> false &&((false)||(false))
*  if (i > 4 && i % 5 == 0 && array1[i] > 0 && array1[i] < 6 || array1[i] > 10 && array1[i] < 20) {
            if (i > 4 && i % 5 == 0 && ((array1[i] > 0 && array1[i] < 6) || (array1[i] > 10 && array1[i] < 20)))
* */

//В двумерном массиве натуральных случайных чисел от 10 до 99 найти количество всех двухзначных чисел,
// у которых сумма цифр кратная 2
// 13, 11

class HW16_11_19_2 {
    public static void main(String[] args) {
        Random random = new Random();

        int[][] array = new int[3][3];

//        System.out.println(random.nextInt(90) + 10);//10 - 99 nextInt(max - min + 1) + min
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                array[i][j] = random.nextInt(90) + 10;
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                int dozens = array[i][j] / 10;
                int once = array[i][j] % 10;

                if ((dozens + once) % 2 == 0) {

                    System.out.print(array[i][j] + "\t");
                }
            }
        }

    }
}

class HW16_11_19_3 {
    public static void main(String[] args) {
        Random random = new Random();

        int[][] array = new int[3][3];

//        System.out.println(random.nextInt(90) + 10);//10 - 99 nextInt(max - min + 1) + min
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                array[i][j] = random.nextInt(90) + 10;
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }

        int sumFirstRow = 0;
        int sumLastRow = 0;


        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i == 0) {
                    sumFirstRow += array[i][j];
                }

                if (i == array.length - 1) {
                    sumLastRow += array[i][j];
                }
            }
        }

        System.out.println(sumFirstRow * 1.0 / array[0].length);
        System.out.println(sumLastRow * 1.0 / array[array.length - 1].length);

        /*for (int j = 0; j < array.length; j++) {//
            sumFirstRow += array[0][j];

            sumLastRow += array[array.length - 1][j];

        }*/



    }
}

/*
 * 23 / 10 -> 2
 * 23 % 10 = 3
 * 23 - 2 * 10 = 3
 * */

/*
17	79	60 i = 0
26	67	82
23	80	94 i = 2
* */