package homeworks.two_dim_array;

public class WorkWithRow {
    public static void main(String[] args) {
        int[][] array = {
                {26, 65, 77, 91},
                {75, 13, 96, 99},
                {55, 24, 73, 92},
                {40, 17, 22, 17}
        };

        for (int i = 0; i < array[0].length; i++) {

            int sumColumn = 0;

            for (int j = 0; j < array.length; j++) {
                sumColumn += array[j][i];
            }

            System.out.println("Sum " + i + " column = " + sumColumn);
        }

        /*
        * i = 0;
        * array[0][0]; j = 0
        * array[1][0]; j = 1
        * array[2][0]
        * array[3][0]
        * i = 1;
        * array[0][1]; j = 0
        * array[1][1]; j = 1
        * array[2][1]
        * array[3][1]
        *
        * */

    }
}
