package homeworks.two_dim_array;

import java.util.Random;

//Создайте массив из 12 случайных целых чисел из отрезка [-15;15].
// Определите какой элемент является в этом массиве максимальным и сообщите индекс его последнего вхождения в массив.
public class HW07_12_19_1 {
    public static void main(String[] args) {
        Random random = new Random();

        int[] array = new int[12];

        for (int i = 0; i < array.length; i++) {

            array[i] = random.nextInt(31) - 15;
            System.out.print(array[i] + "\t");
        }
        System.out.println();

        int max = array[0];
        int index = -1;


        for (int i = 0; i < array.length; i++) {//1, 2, 3, 4
            max = array[0];
            if (array[i] >= max) {
                max = array[i];
                index = i;
            }

        }

        System.out.println("Index = " + index);

        /*for (int i = 0; i < array.length; i++) {// break, continue

            if (array[i] == max) {

                index = i;

            }
            if (index != -1) {
                System.out.println(index);


            }


        }*/
    }
}
