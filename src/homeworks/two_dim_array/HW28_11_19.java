package homeworks.two_dim_array;

import java.util.Random;

public class HW28_11_19 {
    public static void main(String[] args) {
//        В двумерном массиве Вывести максимальный,  минимальный элемент и среднее арифметическое каждой строки

        Random random = new Random();

        int[][] array = new int[3][3];

        for (int i = 0; i < array.length; i++) {

            for (int j = 0; j < array[0].length; j++) {
                array[i][j] = random.nextInt(5);

                System.out.print(array[i][j] + "\t");
            }

            System.out.println();
        }

        int max = array[0][0];
        int min = array[0][0];
        int sum = 0;

        for (int i = 0; i < array.length; i++) {

            max = array[i][0];
            min = array[i][0];

            for (int j = 0; j < array[0].length; j++) {
                if (array[i][j] > max) {
                    max = array[i][j];
                }

                if (array[i][j] < min) {
                    min = array[i][j];
                }
                sum += array[i][j];
            }

            System.out.println("Row = " + i + "\tMax = " + max + "\tMin = " + min + "\tAvg = " + sum * 1.0 / array.length);
        }
    }
}

class HW28_11_19_1 {
    public static void main(String[] args) {
// Создайте массив из всех чётных чисел от 2 до 20 и выведите элементы массива на экран сначала в строку,
// отделяя один элемент от другого пробелом, а затем в столбик (отделяя один элемент от другого началом новой строки).

        /*int length = 11;
        int[] array = new int[length];

        for (int i = 0; i < array.length; i++) {
            array[i] = i * 2;

//            System.out.print(array[i] + "\t");

            System.out.println(array[i] + "\t");
        }*/

        int a = 0;
        for (int i = 2; i <= 20; i++) {
            if (i % 2 == 0) a++;
        }

        int[] Mas = new int[a];
        for (int i = 2, b = 0; i <= 20; i++) {
            if (i % 2 == 0) {
                Mas[b] = i;
                System.out.print(Mas[b] + " ");
                b++;
            }
        }
        System.out.println(" ");
        for (int i = 0; i < Mas.length; i++) {
            System.out.println(Mas[i]);
        }
    }
}

//Создайте массив из всех нечётных чисел от 1 до 99, выведите его на экран в строку,
// а затем этот же массив выведите на экран тоже в строку, но в обратном порядке (99 97 95 93 … 7 5 3 1).
class HW28_11_19_2 {
    public static void main(String[] args) {

        /*int length = 50;

        int[] array = new int[length];

        for (int i = 0; i < array.length; i++) {
            array[i] = i * 2 + 1;

            System.out.print(array[i] + " ");


        }*/

        int a = 0;
        for (int i = 1; i <= 99; i++) {
            if (i % 2 != 0) a++; /*i%2!= 0 - не четное число*/
        }

        int[] array = new int[a];
        for (int i = 1, b = 0; i <= 99; i++) {
            if (i % 2 != 0) {
                array[b] = i;
                System.out.print(array[b] + " ");
                b++;
            }
        }
        System.out.println();

        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
    }
}

class HW28_11_19_3 {
    public static void main(String[] args) {
//Создайте массив из 15 случайных целых чисел из отрезка [0;9].
// Выведите массив на экран. Подсчитайте сколько в массиве чётных элементов и выведете это количество
// на экран на отдельной строке.
        Random random = new Random();
        int a = 0;
        int[] array = new int[15];
        for (int i = 0; i < array.length; i++) {

            array[i] = random.nextInt(10);//0-9(max - min + 1) + min
            System.out.print(array[i] + "\t");
            if (array[i] > 0 & array[i] % 2 == 0) a++;
        }
        System.out.println();
        System.out.println("Всего в масиве " + a + " четных элементов.");
    }
}


//    Создайте массив из 8 случайных целых чисел из отрезка [1;10].
//    Выведите массив на экран в строку. Замените каждый элемент с нечётным индексом на ноль.
//    Снова выведете массив на экран на отдельной строке.
class HW28_11_19_4 {
    public static void main(String[] args) {

        Random random = new Random();

        int[] array = new int[8];
        for (int i = 1; i < array.length; i++) {
            array[i] = random.nextInt(11);//1-10(max - min + 1) + min
            System.out.print(array[i] + "\t");

        }

        System.out.println();

        for (int i = 1; i < array.length; i++) {

            if (array[i] > 0 & array[i] % 2 != 0) array[i] = 0;

            System.out.print(array[i] + "\t");
        }
    }
}

/*Создайте 2 массива из 5 случайных целых чисел из отрезка [0;5] каждый,
выведите массивы на экран в двух отдельных строках. Посчитайте среднее арифметическое элементов каждого массива
и сообщите, для какого из массивов это значение оказалось больше
(либо сообщите, что их средние арифметические равны).*/
class HW28_11_19_5 {
    public static void main(String[] args) {
        Random random = new Random();

        int[] array1 = new int[5];
        int[] array2 = new int[5];

        int sum1 = 0;
        int sum2 = 0;


        for (int i = 0; i < array1.length; i++) {
            array1[i] = random.nextInt(6);
            System.out.print(array1[i] + "\t");
        }
        System.out.println();

        for (int i = 0; i < array2.length; i++) {
            array2[i] = random.nextInt(6);
            System.out.print(array2[i] + "\t");
        }

        for (int i = 0; i < array1.length; i++) {
            sum1 += array1[i];
            sum2 += array2[i];
        }

        double avgArray1 = sum1 * 1.0 / array1.length;
        double avgArray2 = sum2 * 1.0 / array1.length;
        System.out.println("avgArray1 = " + avgArray1 + "\tavgArray2 = " + avgArray2);

        System.out.println();

        double maxAvg = 0;

        String name = "";

        if (avgArray1 > avgArray2) {
             maxAvg = avgArray1;
             name = "array1";
        } else if (avgArray1 < avgArray2) {
            maxAvg = avgArray2;
            name = "array2";
        }

        if (avgArray1 == avgArray2) {
            System.out.print("AvgArrays are equal");
        } else {
            System.out.println("MaxAvg = " + maxAvg + " for array with name " + name);
        }

    }
}

/*Создайте массив из 4 случайных целых чисел из отрезка [10;99],
выведите его на экран в строку. Определить и вывести на экран сообщение о том,
является ли массив строго возрастающей последовательностью.*/
class HW28_11_19_6 {
    public static void main(String[] args) {
        Random random = new Random();
        int[] array = new int[4];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(90) + 10;

            System.out.print(array[i] + "\t");
        }

        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                if (array[i - 1] >= array[i]) {
                    System.out.println("Последовательноть не возрастающая");
                    break;
                }
            }

            if (i == array.length - 1) {
                System.out.println("Последовательноть возрастающая");
            }

        }

    }
}