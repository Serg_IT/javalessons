/*Создать имитацию коллекции(с помощью массива) ArrayList для работы с типом int.
Создать класс с полем типа одномерный массив.
Класс должен выполнять следующие операции:
1) добавление элементов.+
2) изменение/удаление элементов по индексу. +
3) увеличение длины массива на заданное количество элементов.+
4) уменьшение длины массива до заданного количество элементов. ww
5) вывод элементов в консоль в прямом и обратном порядке. ww
6) сортировка массива методом пузырька(http://study-java.ru/uroki-java/urok-11-sortirovka-massiva/). +
7) добавление массива в массив(контактенация). 1 2 3 0 0 + 7 8 9 -> 1 2 3 7 8 9 0  +
8) удалять дубликаты. 1 2 3 2 1 5 8 -> 1 2 3 5 8  +
9) Поиск первой позиции элемента методом линейного поиска.www
10) Перемешивание элементов листа в случайном порядке. 1 2 3 2 1 5 8 -> 3 2 5 8 2 1 +
При удалении элемента не обнулять его, а удалять физически.
Начальную размерность листа юзер вводит с консоли. -
Создать меню для работы с листом из консоли. -
Условие добавления: перезаписывать если элемент равен 0;
В задаче не использовать методы класса Arrays, System и коллекции.*/

package homeworks.imitation_arraylist;

import java.util.Random;

public class ImitationArrayList {
    private int[] array;

    public ImitationArrayList() {
        this.array = new int[3];//0
    }

    //TODO Only for test purpose
    public int[] getArray() {
        return array;
    }

    //TODO Only for test purpose
    public void setArray(int[] array) {
        this.array = array;
    }

    public void addElement(int value) {
        resize();

        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                array[i] = value;
                break;
            }
        }

    }

    private void resize() {
        if (array[array.length - 1] != 0) {
            increaseArray(array.length * 2);
        }
    }

    private void resize1() {

        int countZeros = 0;
        boolean flag = true;

        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                ++countZeros;
                flag = false;
            }
        }

        if (array[array.length - 1] != 0) {
            increaseArray(array.length * 2);
        }
    }

    public void changeElementByIndex(int value, int index) {
        if (index >= array.length || index < 0 || value == 0) {
            System.out.println("Incorrect index or value == 0");
            return;
        }

        array[index] = value;
//        array[1] = 8
    }

    public void deleteElementByIndex(int index) {
        if (index >= array.length || index < 0) {
            System.out.println("Incorrect index");
            return;
        }

        int[] tempArray = new int[array.length - 1];

        for (int i = 0; i < index; i++) {
            tempArray[i] = array[i];
        } //5, 8

        for (int i = index; i < array.length - 1; i++) {//tempArray[2] = array[3]
            tempArray[i] = array[i + 1];//tempArray[2] = array[3]
        }

        array = tempArray;

    }

    public void increaseArray(int size) {

        int[] tempArray = new int[array.length + size];//0

        for (int i = 0; i < array.length; i++) {
            tempArray[i] = array[i];
        }

        array = tempArray;
    }

    public void decreaseArray(int size) {//3
        int[] tempArray = new int[array.length - size];//7

        for (int i = 0; i < tempArray.length; i++) {//10
            tempArray[i] = array[i];
        }

        array = tempArray;
    }

    public void bubbleSortArray() {

        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }

        }

        printRightOrder();

    }

    public void linearSearch(int elementToSearch) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == elementToSearch) {
                System.out.println(i);//or sout elementToSearch
                break;
            }
        }
    }

//  1, 2, 9, 8, 5, 0, 0, 0, 0, 0 -> 1, 2, 9, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0

    public void printRightOrder() {

        System.out.println();
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }
    }

    public void printReverseOrder() {//Hello olleh

        for (int i = array.length - 1; i >= 0; i--) {//move to separate method
            System.out.print(array[i] + "\t");
        }
    }

    /*
     *  1 2 3 0 0 0 0 + 9 6 8 -> 1 2 3 9 6 8 0
     * 1) Counting zeros = 2
     * 2) CZ >= newArray; CZ < newArray -> newArray.length - CZ;3 - 2 = 1 + 1
     * 3) Add array to array
     * */

    public void addArray(int[] newArray) {
        int countZeros = 0;

        for (int e : array) {
            if (e == 0) {
                ++countZeros;
            }
        }

        int positionFirstZero = array.length - countZeros;// индекс первого встр. 0 в певром массиве

        if (countZeros < newArray.length) {
            increaseArray(newArray.length - countZeros + 1);
        }

        for (int i = positionFirstZero, j = 0; j < newArray.length; i++, j++) {
            array[i] = newArray[j];
        }

    }

    /*
     * 5, 5, 8, 7, 7  -> 5, 8, 7
     * 5, 5, 8, 7, 7  -> 5, 8, 7, 7
     * */

    public void deleteDublicate() {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    deleteElementByIndex(j);
                    --j;
                }
            }
        }
    }

    public void shuffling() {// 1 2 3 0 5 4
        Random random = new Random();

        for (int i = array.length - 1; i > 1; i--) {
            int j = random.nextInt(i);//3

            int temp = array[i];//0
            array[i] = array[j];//0 -> 4
            array[j] = temp;

        }
    }
}

/*
 * 5, 8, 7(предполагаем, что нужно удалить), 9, 5  0  0 -> 5, 8, 9, 5, 0, 0
 *
 *
 *
 * */
