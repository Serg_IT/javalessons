package homeworks.imitation_arraylist;

import java.util.Scanner;

public class MenuList {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {

        ImitationArrayList list = new ImitationArrayList();

        while (true) {
            menu();
            int key = SCANNER.nextInt();
            try {
                switch (key) {
                    case 1: {
                        System.out.println("Enter value");
                        int choose = SCANNER.nextInt();

                        list.addElement(choose);
                        break;
                    }
                    case 2: {
                        System.out.println("Enter value");
                        int value = SCANNER.nextInt();

                        System.out.println("Enter index");
                        int index = SCANNER.nextInt();

                        list.changeElementByIndex(value, index);

                        break;
                    }
                    case 3: {
                        list.printRightOrder();
                        break;
                    }
                    case 4: {

                        System.out.println("Enter index");
                        int index = SCANNER.nextInt();

                        list.deleteElementByIndex(index);

                        break;
                    }
                    case 5: {

                        System.out.println("Enter size");

                        int size = SCANNER.nextInt();

                        list.increaseArray(size);

                        break;
                    }
                    case 6: {

                        System.out.println("Enter size");

                        int size = SCANNER.nextInt();

                        list.decreaseArray(size);

                        break;
                    }
                    case 7: {
                        list.bubbleSortArray();
                        break;
                    }
                      case 8: {
                          System.out.println("Enter value");
                          int value = SCANNER.nextInt();
                          list.linearSearch(value);
                        break;
                    }
                    case 9:
                        return;
                    default:
                        return;
                }
            } catch (Exception e) {
                System.out.println("Exception" + e);
            }
        }
    }

    public static void menu() {
        System.out.println("\n1) Add value.\n" +
                "2) Change value by index.\n" +
                "3) Print array in right order.\n" +
                "4) Delete value by index.\n" +
                "5) Increase size array.\n" +
                "6) Decrease size array.\n" +
                "7) To do bubblesort array.\n" +
                "8) Print index element.\n" +
                "9) Exit.");
    }
}

