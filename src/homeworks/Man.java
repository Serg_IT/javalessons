package homeworks;

public class Man {
    //Создать класс Man с полями (возраст, кол-во детей, рост, вес). Создать 5 объектов(вызвать конструктор)
//и вывести данные о человеке в консоль

    private int age;//access-modifier type name_field;
    int children;
    int height;
    int weight;

    Man(int mAge, int mChild, int mHeight, int mWeight) {//-900
        if (mAge < 0) {
            age = 0;
        } else {
            age = mAge;
        }
        children = mChild;
        height = mHeight;
        weight = mWeight;
    }

    public Man() {//constructor without incoming params or default
        age = 45;
        children = 1;
        height = 150;
        weight = 90;
    }

    // access-modifier return_type or void method_name(incoming params)
    public void setAge(int mAge) {
        if (mAge < 0) {
            age = 0;
        } else {
            age = mAge;
        }
    }

    public int getAge() {
        return age;
    }

    public int getChildren() {
        return children;
    }

    public void setChildren(int mChildren) {
        children = mChildren;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void printInfo() {
        System.out.println("Age - " + age + ", Children - " + children + ", Height - " + height + ", Weight - " + weight);
    }

    /*@Override
    public String toString() {
        return "Man{" +
                "age=" + age +
                ", children=" + children +
                ", height=" + height +
                ", weight=" + weight +
                '}';
    }*/
}

class TestMan {
    public static void main(String[] args) {
//        int a = 36;
//        int c = 2;
//        int h = 185;
//        int w = 87;


        Man dima = new Man(36, 2, 185, 87);
//        Man roman = new Man();
        Man oleg = new Man(25, 3, 175, 75);
        Man kiril = new Man(32, 1, 167, 95);
        Man anton = new Man(46, 2, 180, 105);
        Man david = new Man(39, 2, 182, 99);

/*      dima.age = 36;
        dima.children = 2;
        dima.height = 185;
        dima.weight = 87;*/

        /*System.out.println("Dima: " + "Age - " + dima.age + ", Children - " + dima.children + ", Height - " + dima.height + ", Weight - " + dima.weight);
        System.out.println("Oleg: " + "Age - " + oleg.age + ", Children - " + oleg.children + ", Height - " + oleg.height + ", Weight - " + oleg.weight);
        System.out.println("Kiril: " + "Age - " + kiril.age + ", Children - " + kiril.children + ", Height - " + kiril.height + ", Weight - " + kiril.weight);*/
//        System.out.println(dima);
//        dima.setAge(15);

//        System.out.println(dima.getAge());

        dima.printInfo();
    }
}

class Tree {

    public static void main(String[] args) {
        new Tree();
    }
}
