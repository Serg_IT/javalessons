package homeworks.string;

/*Перевернуть физически строку наоборот(отсортировать) (Hello - > olleH)*/
public class Tasks {
    public static void main(String[] args) {
        String str = "Hello";

        int strLength = str.length();

        for (int i = str.length() - 1; i >= 0; i--) {
            System.out.print(str.charAt(i) + " ");
        }

//        String revers = new StringBuffer(str).reverse().toString();
//        System.out.println(revers);
    }

}

/*Вывести текст, составленный из последних букв всех слов*/
class Tasks1 {
    public static void main(String[] args) {
        String str = "Hello my dear friend";
//        "Hello", "my", "dear", "friend"

        String[] strings = str.split(" ");


        for (int i = 0; i < strings.length; i++) {

            int lastIndex = strings[i].length() - 1;

            System.out.println(strings[i].charAt(lastIndex));
        }


//        char[] result = str.toCharArray();
//        System.out.println("Char array :");
//        for (int i = 0; i < result.length; i++) {
//            System.out.println("Element [" + i + "]: " + result[i]);

    }
}

/*Дано слово, состоящее из четного числа букв .Вывести на экран его первую половину ,не используя оператор цикла*/
class Tasks2 {
    public static void main(String[] args) {
        String str = "Worlds";
        int halfLength = str.length() / 2;

        System.out.println(halfLength);
//        System.out.println(str.substring(0,3));
//        System.out.println(str.substring(halfLength));
        String halfString = str.substring(0, halfLength);
        System.out.println(halfString);


    }
}
