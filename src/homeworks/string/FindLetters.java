package homeworks.string;

/*
* Declare two local variables myStr1 and myStr2 of String type and assign a value “Cartoon”
for first string and ”Tomcat” for second.
Write code to display all of the letters, which are present in the first word, but absent in the second.
* */
public class FindLetters {
    public static void main(String[] args) {

        String myStr1 = "Cartoon".toLowerCase();// с переводом всех букв в нижний регистр
        String myStr2 = "Tomcat".toLowerCase();

        int myStr1Length = myStr1.length();
        int myStr2Length = myStr2.length();

        for (int i = 0; i < myStr1Length; i++) {
            char c = myStr1.charAt(i);

            String s = String.valueOf(c);

            if (!myStr2.contains(s)) {
                System.out.println(c);
            }
        }
    }


}
