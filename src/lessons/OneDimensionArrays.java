package lessons;

public class OneDimensionArrays {
    public static void main(String[] args) {

        int a = 10;

        //data_type[] name_array = new data_type[size];

        int[] arr = new int[10];
        //0 1 2 3 4 5 6 7 8 9
        //0 0 5 0 0 0 0 0 0 0 (?0 - 10)

        arr[2] = 5;
        arr[0] = 10;

        System.out.println(arr[0]);
        System.out.println(arr[2]);


    }
}
