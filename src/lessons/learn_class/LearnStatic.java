package lessons.learn_class;

public class LearnStatic {
    private int age;
    private static String value = "Hello";
    public static final int COUNT_PEOPLE;
    public final int START;

    {
//        START = 8;
        System.out.println("Non-static block initialization");
    }

    static  {
        COUNT_PEOPLE = 9;
        System.out.println("Static block initialization");
    }

    /*{
        System.out.println("Non-static block initialization1");
    }*/

    public LearnStatic(int age) {
        START = 8;
        System.out.println("Constructor");
        String value = getValue();
        this.age = age;
    }

    public int getAge() {
//        START = 8;
        return age;
    }

    public void setAge(final int age) {//9
//        age = 10;
        this.age = age;
    }

    public static String getValue() {
        return value;
    }

    public static void setValue(String value) {
        LearnStatic.value = value;
    }

    public static void main(String[] args) {

    }
}

class TestStatic {
    public static void main(String[] args) {
//        System.out.println(LearnStatic.getValue());

        LearnStatic aStatic = new LearnStatic(8);
        LearnStatic aStatic1 = new LearnStatic(8);

        aStatic = new LearnStatic(9);

//        aStatic.value

    }
}
