package lessons.learn_class;

public class LearnClass {
    int count;//field count
    int width;//field width
    private LearnClass learnClass;

    LearnClass(int c, int w) {//c = 10, w = 15
        count = c;
        width = w;
    }
}

class TestClass {
    public static void main(String[] args) {
        //data_type name_variable = value;
        int a = 10;
        int b = 15;

        int[] array1 = new int[5];

        //data_type name_reference = new data_type();
        LearnClass aClassOne = new LearnClass(10, 15);//LearnClass() - call to constructor

       /* aClassOne.count = 10;
        aClassOne.width = 15;*/

        LearnClass aClassTwo = new LearnClass(18, 19);
/*
        aClassTwo.count = 18;
        aClassTwo.width = 19;*/

        System.out.println("Count = " + aClassTwo.count + "\t" + aClassTwo.width);

    }
}
