package lessons;

public class IncrementDecrement {
    public static void main(String[] args) {
        int count = 2;

        int result = count++ + ++count;//2 + 4

        System.out.println(result);
    }
}
