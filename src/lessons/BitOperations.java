package lessons;

public class BitOperations {
    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString(42));//00101010
        System.out.println(Integer.toBinaryString(17));//00010001
        System.out.println("42 & 17 = " + (42 & 17));     //00000000
        System.out.println("42 | 17 = " + (42 | 17));     //00111011
        System.out.println("42 >> 2 = " + (42 >> 2));     //00101010 = 00001010 = 42 / 2(2) = 42 / 4 = 10
        System.out.println("42 << 2 = " + (42 << 2));     //00101010 = 10101000 = 42 * 2(2) = 42 * 4 = 168
        //2(7)
        int pow = (int) Math.pow(2, 7);

        System.out.println("pow = " + pow);

        System.out.println("1 << 7 = " + (1 << 7));//1 * 2(7)

    }
}
