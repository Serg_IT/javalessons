package lessons.condition;

public class LearnSwitch {
    public static void main(String[] args) {
        int a = 5;

        /*if (a == 10) {
            System.out.println("10");
        }
        if (a == 9) {
            System.out.println("9");
        }
        if (a == 8) {
            System.out.println("8");
        }
        if (a == 6) {
            System.out.println("6");
        }
        if (a == 7) {
            System.out.println("7");
        }*/

        switch (a) {
            case 10:// if(a == 10)
                System.out.println("10");
                break;
            case 9:// if(a == 10)
                System.out.println("9");
                break;

            case 8:// if(a == 10)
                System.out.println("8");
                break;
            default:
                System.out.println("Unknow value");

        }
    }

    public void print(int a) {
        if (a == 10) {
            System.out.println("a = 10");
        }
        if (a == 9) {
            System.out.println("a = 9");
        }
        if (a == 8) {
            System.out.println("8");
        }
        if (a == 6) {
            System.out.println("6");
        }
        if (a == 7) {
            System.out.println("7");
        }
    }
}
