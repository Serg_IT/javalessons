package lessons.condition;

public class Condition {
    public static void main(String[] args) {
        int a = 12;
        int b = 20;

  /*      if (a < 15) {
            System.out.println("a < 15");
        } else if(a == 15) {
            System.out.println("a == 15");
        } else {
            System.out.println("a > 15");
        }
        //  *

   */
        //ternary operator
        // condition ? statemet if condition is true : statemet if condition is false

        int y = a == 20 ? 45 : 65;

            System.out.println(y);

        int y1 = a < 20 ? (a == 10 ? 5 : 9) : 65;

//       System.out.println(y1);

        if (a < 12 && b == 20) {//true && false = 0 & 1 = 1 (?)
            System.out.println();
        }

        if (a == 12 || b > 20) {//false || true = 1 | 0 = 1
            System.out.println();
        }

    }
}
