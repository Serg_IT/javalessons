package lessons;

public class DataTypes {
    public static void main(String[] args) {
//        System.out.println(5 / 2);
//        data_type name_of_variable = value;
        byte a = 125;

        byte b = 10;

        byte c = (byte)(a - b);//115 - int, byte 32 -> 8

        short s = 12345;

        short s1 = 12398;

        short s2 = (short)(s1 + s);

        int i = 123546;

        int m = 45;//8 -> 32

        long l = 12354678L;

        float f = 45.6F;// 3.4 * 10 (-038)

        float f1 = 96.6F;

        float f2 = f + f1;

        System.out.println(f2);

        double d = 12.6d;

        System.out.println("Result 125 - 10 = " + c);// [0, 9)


    }
}

/*
Binary
* 50(10) -> 00110010 (2)
* 50 / 2 = 25, ost 0
* 25 / 2 = 12, ost 1
* 12 / 2 = 6, ost 0
* 6 / 2 = 3, ost 0
* 3 / 2 = 1, ost 1
*
  76543210
* 00110010 (2) -> (10)

0 * 2(7) + 0 * 2(6) + 1 * 2(5) + 1 * 2(4) + 0 * 2(3) + 0 * 2(2) + 1 * 2(1) + 0 * 2(0) = 32 + 16 + 2 = 50
* */
