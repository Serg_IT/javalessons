package lessons.learn_string;

public class LearnString {
    public static void main(String[] args) {
        String str = "Hello llo";//string-literal
        String str1 = new String("Hello");//string object
//        String str1 = str;

     /*   System.out.println("str == str1 -> " + (str == str1.intern()));
        System.out.println("str == str1 -> " + (str == str1));*/
//        System.out.println("str == str1 -> " + (str.equals(str1)));
/*
        System.out.println(1 + 2 + "3");//33
        System.out.println("3" + 1 + 2);//
        System.out.println(1 + "2" + 3);//123*/

        char x = str.charAt(0);

//        System.out.println(x);
//        System.out.println(str.substring(2));
        System.out.println(str.substring(2, 4));

        String s = str.concat("World");
//        System.out.println(str);

        int lIndex = str.lastIndexOf("l");
        int index = str.indexOf("l");

        System.out.println(index);
//        System.out.println(str.contains(String.valueOf('H')));

    }
}
