package lessons.learn_string;
public class ExampleString {
    public static void main(String[] args) {
        String sentence = "Hello my dear friend";

        String[] s = sentence.split(" ");//"my dear" -> s.length 2 // maxLength minLength length every String by array

        int maxLength = s[0].length();
        int minLength = s[0].length();

        String max = null;
        String min = null;

        for (int i = 0; i < s.length; i++) {
            if (s[i].length() > maxLength) {
                maxLength = s[i].length();
                max = s[i];
            }

            if (s[i].length() < minLength) {
                minLength = s[i].length();
                min = s[i];

            }
        }
        System.out.println("Max = " + maxLength + "\nMin = " + minLength);
        System.out.println("Max word = " + max + "\nMin word = " + min);
//        System.out.println(s.length);

        StringBuilder builder = new StringBuilder("World");

        String str = "World";
        str = "World0";
        str = "World0123456789";

        for (int i = 0; i < 10; i++) {
            str += i;
            builder.append(i);
        }

        /*
        * iter0 -> str = str(World) + 0 = World0
        * iter1 -> str = str(World0) + 1 = World01
        * iter2 -> str = str(World01) + 2 = World012
        * */

//        System.out.println(builder.toString());

    }
}
