package lessons.learn_string;

/*
 * Соединить две строки след. вида: ("abc", "drf") результат: ("adbrcf"), ("ab", "drf") результат: ("adbrf"), ("abc", "dr") результат: ("adbrc")
 * */
public class TaskReview {
    public static void main(String[] args) {
        String s1 = "abc";
        String s2 = "drt";

        if (s1.length() < s2.length()) {
            int length = s2.length() - s1.length();

            for (int i = 0; i < length; i++) {
                s1 += " ";
            }
        } else {
            int length = s1.length() - s2.length();
            for (int i = 0; i < length; i++) {
                s2 += " ";
            }
        }

        String result = "";

        char c = ' ';//'' символ char, юникод ('\u0125')
        String s = "S";
        for (int i = 0; i < s1.length(); i++) {
            String str1 = String.valueOf(s1.charAt(i));
            String str2 = String.valueOf(s2.charAt(i));

            result = result.concat(str1).concat(str2);//"Hello" + String.valueOf('p') -> "p"
        }

        result = result.replaceAll(" ", "");//replaceAll - метод замены пробела " " на пустую строчку "".

        System.out.println(result);

        /*StringBuilder builder = new StringBuilder();

        for (int i = 0; i < s1.length(); i++) {
            builder.append(s1.charAt(i)).append(s2.charAt(i));
        }*/

//        System.out.println(builder.toString());

//        char r = '\u0152';


    }
}

class TaskReview1 {
    public static void main(String[] args) {
        String s1 = "abc";// -> "ab "
        String s2 = "dr";

        StringBuilder b1 = new StringBuilder(s1);

//        |3| = 3, |-3| = 3

        int length = Math.abs(s1.length() - s2.length());//метод вычесления модуля//длины"abc" - "dr" = 1

        b1 = s1.length() > s2.length() ? new StringBuilder(s2) : b1;//

        /*if (s1.length() > s2.length()) {
            b1 = new StringBuilder(s2);
        }*/

        for (int i = 0; i < length; i++) {//"abc" - "dr" = 1 = length
            b1.append(" ");//"ab "
        }

        if (s1.length() > s2.length()) {
            s2 = b1.toString();// переинициализируем (добавляем пробел)
        } else {
            s1 = b1.toString();
        }

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < b1.length(); i++) {
            builder.append(s1.charAt(i)).append(s2.charAt(i));
        }

        System.out.println(builder.toString().replaceAll(" ", ""));

    }
}
