import homeworks.imitation_arraylist.ImitationArrayList;
import org.junit.*;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class ImitationArrayListTest {

    private static ImitationArrayList list;// 10 elements -> 0

    @BeforeClass
    public static void initializeList() {
        list = new ImitationArrayList();
    }


    @Rule
    public SystemOutRule outRule = new SystemOutRule().enableLog();

    @Before
    public void clearArray() {
        list.setArray(new int[3]);//0 0 0
    }

    @Test
    public void shouldAddElementToArray() {
        list.addElement(5);
        list.addElement(7);

        int[] array = list.getArray();

        Assert.assertEquals(array[0], 5);
        Assert.assertEquals(array[1], 7);
    }

    @Test
    public void shouldChangeElementByIndex() {
        list.addElement(6);
        list.addElement(3);

        list.changeElementByIndex(23, 0);

        int[] array = list.getArray();

        Assert.assertEquals(array[0], 23);

    }

    @Test
    public void shouldIncreaseArray() {
        list.addElement(8);
        list.addElement(3);

        list.increaseArray(10);

        int[] array = list.getArray();

        Assert.assertEquals(array.length, 13);
        Assert.assertEquals(array[0], 8);
        Assert.assertEquals(array[1], 3);

    }

    @Test
    public void shouldDecreaseArray() {
        list.addElement(3);
        list.addElement(3);
        list.addElement(3);

        list.decreaseArray(1);

        int[] array = list.getArray();

        Assert.assertEquals(array.length, 2);
        /*Assert.assertEquals(array[0], 3);
        Assert.assertEquals(array[1], 3);*/

        Assert.assertArrayEquals(array, new int[]{3, 3});

    }

    @Test
    public void shouldResizeArray() {
        list.setArray(new int[1]);

        list.addElement(1);
        list.addElement(2);
        list.addElement(3);

        int[] array = list.getArray();

        Assert.assertEquals(array.length, 3);//increaseArray(array.length * 2), array.length + size
        Assert.assertEquals(array[0], 1);
        Assert.assertEquals(array[1], 2);
        Assert.assertEquals(array[2], 3);
    }

    @Test
    public void shouldDeleteDublicate() {
        list.addElement(5);
        list.addElement(1);
        list.addElement(5);
        list.addElement(7);

        list.deleteDublicate();

        int[] array = list.getArray();

        Assert.assertEquals(array.length, 4);
        Assert.assertEquals(array[0], 5);
        Assert.assertEquals(array[1], 1);
        Assert.assertEquals(array[2], 7);
        Assert.assertEquals(array[3], 0);
    }

    @Test
    public void shouldPrintRightOrder() {//use rule
        list.addElement(1);
        list.addElement(2);
        list.addElement(3);

        list.printRightOrder();

        String log = outRule.getLog();

        Assert.assertTrue(log.contains("1\t2\t3\t"));
    }

    @Test
    public void shouldPrintReverseOrder() {//use rule
        list.addElement(1);
        list.addElement(2);
        list.addElement(3);

        list.printReverseOrder();

        String log = outRule.getLog();

        Assert.assertTrue(log.contains("3\t2\t1\t"));
//        int[] array = list.getArray();
//
//        Assert.assertEquals(array.length, 3);
//        Assert.assertEquals(array[0], 1);
//        Assert.assertEquals(array[1], 2);
//        Assert.assertEquals(array[2], 3);
    }

    @Test
    public void shouldAddArray() {
        list.addElement(1);
        list.addElement(1);
        list.addElement(1);

        list.addArray(new int[]{2, 2, 2});

        int[] array = list.getArray();

        Assert.assertArrayEquals(array, new int[]{1, 1, 1, 2, 2, 2, 0});

//        Assert.assertEquals(array.length, 7);//increaseArray(newArray.length - countZeros + 1);
//        Assert.assertEquals(array[0], 1);
//        Assert.assertEquals(array[1], 1);
//        Assert.assertEquals(array[2], 1);
//        Assert.assertEquals(array[3], 2);
//        Assert.assertEquals(array[4], 2);
//        Assert.assertEquals(array[5], 2);
//        Assert.assertEquals(array[6], 0);
    }

    @Test
    public void shouldDeleteElementByIndex() {
        list.addElement(1);
        list.addElement(2);
        list.addElement(3);

        list.deleteElementByIndex(1);

        int[] array = list.getArray();

        Assert.assertEquals(array.length, 2);
        Assert.assertEquals(array[0], 1);
        Assert.assertEquals(array[1], 3);
    }

    @Test
    public void shouldBubbleSortArray() {
        list.addElement(9);
        list.addElement(8);
        list.addElement(7);

        list.bubbleSortArray();

        int[] array = list.getArray();

        Assert.assertEquals(array.length, 3);
        Assert.assertEquals(array[0], 7);
        Assert.assertEquals(array[1], 8);
        Assert.assertEquals(array[2], 9);
    }

    @Test
    public void shouldLinearSearchArray() {//use rule
        list.addElement(4);
        list.addElement(5);
        list.addElement(9);

        list.linearSearch(5);
        String log = outRule.getLog();

        Assert.assertTrue(log.contains("1\t"));
//        int[] array = list.getArray();
//
//        Assert.assertEquals(array.length, 3);
//        Assert.assertEquals(array[0], 4);
//        Assert.assertEquals(array[1], 5);
//        Assert.assertEquals(array[2], 9);
    }

    @AfterClass
    public static void clearList() {
        list = null;
    }

}
