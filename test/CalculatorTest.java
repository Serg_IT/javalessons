import lessons.for_test.Calculator;
import org.junit.*;

public class CalculatorTest {

   private Calculator calculator;


    @BeforeClass
    public static void runBeforeAllTests() {
        System.out.println("Before all tests");
    }

    @Before
    public void runBeforeEachTest() {
        System.out.println("Before each test");
        calculator = new Calculator();
    }

    @Test
    public void shouldReturnSumValues() {

        int sum = calculator.sum(5, 2);

        Assert.assertEquals("Should return 9", 9, sum);
//        Assert.assertTrue(true);
    }

    @Test
    @Ignore
    public void shouldReturnSumValues1() {

        int sum = calculator.sum(5, 2);

        Assert.assertEquals("Should return 9", 19, sum);
//        Assert.assertTrue(true);
    }

    @After
    public void runAfterEachTest() {
        System.out.println("After each test");
    }

    @AfterClass
    public static void runAfterAllTests() {
        System.out.println("After all tests");
    }
}

/*
* A {B()}
* private B
*
* */
