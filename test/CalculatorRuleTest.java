import lessons.for_test.Calculator;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class CalculatorRuleTest {

    @Rule
    public SystemOutRule outRule = new SystemOutRule().enableLog();

    @Test
    public void shouldTestConsole() {
        Calculator calculator = new Calculator();

        calculator.print();

        String log = outRule.getLog();

//        Assert.assertEquals("Hello world\r\n", log);

        Assert.assertTrue(log.contains("Hello world"));
    }
}
